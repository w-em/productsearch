const mix = require('laravel-mix')
const path = require('path')
require('laravel-mix-merge-manifest')
require('vuetifyjs-mix-extension')
const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin')
// const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin')
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.copyDirectory('resources/images', 'public/images')
// const fullPublicPath = path.resolve(__dirname, 'public')
mix.webpackConfig({
  /*
  optimization: {
    runtimeChunk: {
      name: '/js/manifest'
    },
    splitChunks: {
      cacheGroups: {
        vendors: false,
        default: false,
        1: {
          name: 'js/vuetify', // part of the bundle name and
          test (module) {
            if (module.context) {
              // node_modules are needed
              const isNodeModule = module.context.includes('node_modules')
              const nodesToBundle = [
                'vuetify'
              ].some(str => module.context.includes(str))
              if (isNodeModule && nodesToBundle) {
                return true
              }
            }
            return false
          },
          chunks: 'all',
          minSize: 0
        },
        2: {
          name: 'js/vendor',
          test (module) {
            if (module.context) {
              // node_modules are needed
              const isNodeModule = module.context.includes('node_modules')
              // but only specific node_modules
              const nodesToBundle = [
                'axios',
                'swiper'
              ].some(str => module.context.includes(str))
              if (isNodeModule && nodesToBundle) {
                return true
              }
            }
            return false
          },
          chunks: 'all',
          minSize: 0
        },
        3: {
          name: 'js/vendor_lodash',
          test (module) {
            if (module.context) {
              // node_modules are needed
              const isNodeModule = module.context.includes('node_modules')
              // but only specific node_modules
              const nodesToBundle = [
                'lodash'
              ].some(str => module.context.includes(str))
              if (isNodeModule && nodesToBundle) {
                return true
              }
            }
            return false
          },
          chunks: 'all',
          minSize: 0
        },
        4: {
          name: 'js/vendor_vue',
          test (module) {
            if (module.context) {
              // node_modules are needed
              const isNodeModule = module.context.includes('node_modules')
              // but only specific node_modules
              const nodesToBundle = [
                'node_modules/vue/dist',
                'node_modules/vuex-router-sync',
                'node_modules/vue-router',
                'node_modules/vuex-persist',
                'node_modules/vuex',
                'node_modules/vue-meta',
                'node_modules/vue-awesome-swiper'
              ].some(str => module.context.includes(str))
              if (isNodeModule && nodesToBundle) {
                return true
              }
            }
            return false
          },
          chunks: 'all',
          minSize: 0
        }

      }
    }
  }, */
  /*
  output: {
    path: fullPublicPath,
    publicPath: './public/js/',
    filename: '[name].js',
    chunkFilename: '[name].js'
  },
  optimization: {
    minimize: mix.inProduction(),
    splitChunks: {
      minSize: 20000,
      maxSize: 24400
    }
  },
  optimization: {
    minimize: mix.inProduction(),
    // this comes from the .extract() method we are commenting out
    runtimeChunk: {
      name: '/js/manifest'
    },
    splitChunks: {
      chunks: 'all',
      maxInitialRequests: Infinity,
      minSize: 200000,
      maxSize: 1000000,
      cacheGroups: {
        default: false,
        vendors: false,
        search: {
          chunks: 'initial',
          name: 'search566',
          filename: 'js/search123.js'
        },
        vendor: {
          test: /node_modules/,
          chunks: 'initial',
          name: 'vendor',
          filename: 'js/[name].js'
        }
      }

    }
  }, */
  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.(js|vue)$/,
        loader: 'eslint-loader',
        exclude: /node_modules/
      },
      {
        test: /\.?js$/,
        exclude: /(bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ]
  },
  plugins: [
    // new BundleAnalyzerPlugin()
    new SWPrecacheWebpackPlugin({
      cacheId: 'pwa',
      filename: 'service-worker.js',
      staticFileGlobs: ['public/**/*.{css,eot,svg,ttf,woff,woff2,js,html}'],
      minify: true,
      stripPrefix: 'public/',
      handleFetch: true,
      dynamicUrlToDependencies: { // you should add the path to your blade files here so they can be cached
        // and have full support for offline first (example below)
        // '/': ['resources/layouts/app.twig']
        // '/posts': ['resources/views/posts.blade.php']
      },
      staticFileGlobsIgnorePatterns: [/\.map$/, /mix-manifest\.json$/, /manifest\.json$/, /service-worker\.js$/],
      navigateFallback: '/',
      runtimeCaching: [
        {
          urlPattern: /^https:\/\/fonts\.googleapis\.com\//,
          handler: 'cacheFirst'
        }
      ]
      // importScripts: ['./js/push_message.js']
    })
  ],
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.min.js',
      'vue': 'vue/dist/vue.min.js',
      src: path.resolve(__dirname, './resources/js'),
      public: path.resolve(__dirname, '../public/'),
      '@': path.resolve(__dirname, './resources/js')
    }
  }
})
mix.options({
  extractVueStyles: false,
  processCssUrls: true,
  terser: {},
  purifyCss: true,
  postCss: [
    require('autoprefixer')({
      overrideBrowserslist: [
        '> 0.1%',
        'last 7 versions',
        'Android >= 4',
        'Firefox >= 20',
        'iOS >= 8'
      ],
      flexbox: true
    })
  ],
  uglify: {
    uglifyOptions: {
      compress: {
        drop_console: mix.inProduction(),
        drop_debugger: mix.inProduction()
      }
    }
  },
  clearConsole: false,
  cssNano: {
    discardComments: {
      removeAll: true
    }
  }
})

if (mix.inProduction()) {
  if (process.env.COMP_TYPE === 'css') {
    mix
      .sass('resources/sass/pdf.scss', 'public/css')
      .sass('resources/sass/base.scss', 'public/css')
      .version()
      .mergeManifest()
  } else {
    mix.js('resources/js/search.js', 'public/js')
      .extract()
      .version()
  }
} else {
  // Enable sourcemaps
  mix.js('resources/js/search.js', 'public/js')
    .sass('resources/sass/pdf.scss', 'public/css')
    .sass('resources/sass/base.scss', 'public/css')
    .sourceMaps()
    .browserSync({
      proxy: 'localhost'
    })
}

// console.log(mix.dump())
