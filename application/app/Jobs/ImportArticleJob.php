<?php

namespace App\Jobs;

use App\Models\Article;
use App\Models\ArticleCategory;
use App\Models\ArticleFeature;
use App\Models\ArticleGroup;
use App\Models\Category;
use App\Models\Feature;
use App\Models\FeatureClass;
use App\Models\FeatureValue;
use App\Models\Media;
use App\Models\MediaReference;
use App\Services\ImportService;
use App\Services\RequestService;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ImportArticleJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 25;

    /**
     * The maximum number of exceptions to allow before failing.
     *
     * @var int
     */
    public $maxExceptions = 3;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 60;

    /**
     * @var array
     */
    private $curlResult = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($curlResult)
    {
        $this->curlResult = $curlResult;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->curlResult;
        $fieldData = $data->fieldData;
        $portalData = $data->portalData;
        $etimData = $data->portalData->ETIM_Daten;

        $article = $this->importArticleData($fieldData);

        $this->importEtimData($article, $etimData);
        $this->importAdditionalData($article, (array) $fieldData);
        $this->upsertCategory($article, $fieldData);

        if (!empty(trim($fieldData->Montageanleitung))) {
            $this->importAssemblyInstructions($article, $fieldData->Montageanleitung);
        }

        if (!empty($portalData->n2n_Artikel_Bilder) && count($portalData->n2n_Artikel_Bilder) > 0) {
            $this->upsertImages($article, $portalData->n2n_Artikel_Bilder);
        }
    }

    /**
     * @param Article $article
     * @param $filename
     */
    private function importAssemblyInstructions(Article $article, $filename) {
        $url = 'https://dbserv.ddns.net:444/Montageanleitungen/'. $filename . '?login=download:';
        $dbField = Str::slug('Montageanleitung');

        $publicPath = $dbField . '/' . $filename;
        $newFilePath = storage_path('app/public/' . $publicPath);
        if (!file_exists($newFilePath)) {
            $result = RequestService::downloadFile($url);
        }

        if (!is_dir(storage_path('app/public/' . $dbField))) {
            Storage::makeDirectory('public/' . $dbField);
        }

        if (file_exists(storage_path('app/download/' . $filename))) {
            File::move(storage_path('app/download/' . $filename), $newFilePath);
        }

        if (file_exists($newFilePath)) {
            $filesize = filesize($newFilePath);

            if ($filesize === false) {
                File::delete($newFilePath);
            }
            else {
                $info = pathinfo($newFilePath);
                $ext = $info['extension'];
                $media = Media::updateOrCreate(
                    [
                        'path' => $publicPath
                    ], [
                        'title' => $filename,
                        'file_type' => $ext,
                        'file_width' => 0,
                        'file_height' => 0,
                        'file_size' => $filesize
                    ]
                );
                // create media reference to article
                MediaReference::updateOrCreate(
                    [
                        'entity_id' => $article->id,
                        'entity_type' => Article::class,
                        'media_id' => $media->id,
                        'field' => $dbField
                    ], [
                        'sorting' => 1,
                    ]
                );
            }

        }
    }

    /**
     * @param Article $article
     * @param $data
     */
    protected function upsertProduct(Article $article, $data) {

        if (count($data->portalData->n2n_Produkte_Artikel) > 0) {
            foreach($data->portalData->n2n_Produkte_Artikel as $n2n) {
                ImportProductJob::dispatch($n2n->{'n2n_Produkte_Artikel::ID_Produkte'});
            };
        }
    }

    /**
     * @param $article
     * @param $filename
     */
    protected function importDefaultImage($article, $filename) {
        $firstChar = substr($filename, 0, 1);
        $url = 'https://dbserv.ddns.net:444/Previews/Bestell/'.strtoupper($firstChar).'/' . $filename . '?login=download:';
        $dbField = Str::slug('Bestellbild');

        $publicPath = $dbField . '/' . $filename;
        $newFilePath = storage_path('app/public/' . $publicPath);
        if (!file_exists($newFilePath)) {
            try {
                $result = RequestService::downloadFile($url);
            } catch (ClientException $exception) {
                $info = pathinfo($url);
                $url = 'https://dbserv.ddns.net:444/Bestell/'.strtoupper($firstChar).'/' . $info['filename'] . '.tif?login=download:';
                $filename = $info['filename'] . '.tif';
                $publicPath = $dbField . '/' . $filename;
                $newFilePath = storage_path('app/public/' . $publicPath);
                $result = RequestService::downloadFile($url);
            }

        }

        if (!is_dir(storage_path('app/public/' . $dbField))) {
            Storage::makeDirectory('public/' . $dbField);
        }

        $downloadPath = storage_path('app/download/' . $filename);

        if (file_exists($downloadPath)) {
            File::move($downloadPath, $newFilePath);
        }

        if (file_exists($newFilePath)) {
            $data = getimagesize($newFilePath);

            if ($data === false) {
                File::delete($newFilePath);
                File::delete($downloadPath);
            }
            else {
                $info = pathinfo($newFilePath);
                $ext = $info['extension'];
                $width = $data[0];
                $height = $data[1];
                $media = Media::updateOrCreate(
                    [
                        'path' => $publicPath
                    ], [
                        'title' => $filename,
                        'file_type' => $ext,
                        'file_width' => $width,
                        'file_height' => $height,
                        'file_size' => filesize($newFilePath),
                    ]
                );
                // create media reference to article
                MediaReference::updateOrCreate(
                    [
                        'entity_id' => $article->id,
                        'entity_type' => Article::class,
                        'media_id' => $media->id,
                        'field' => $dbField
                    ], [
                        'sorting' => 1,
                    ]
                );
            }

        }
    }

    /**
     * import all images from given article
     * @param Article $article
     * @param array $images
     */
    protected function upsertImages (Article $article, array $images) {
        $schlagwortVariable = "n2n_Artikel_Bilder::Schlagwort_01";
        $imageVariable = "Bilder_Schlagwort::URL_LowRes";
        $sortings = [];

        foreach ($images as $image) {
            $url = $image->$imageVariable;
            $pathInfo = pathinfo($url);
            $urlEx = explode('@', $pathInfo['dirname']);
            $url = 'https://' . $urlEx[1] . DIRECTORY_SEPARATOR . $pathInfo['basename'] . '?login=download:';
            $filename = $pathInfo['basename'];

            $dbField = Str::slug($image->$schlagwortVariable);
            $publicPath = $dbField . '/' . $filename;
            $newFilePath = storage_path('app/public/' . $publicPath);

            if (!file_exists($newFilePath)) {
              try {
                RequestService::downloadFile($url);
              } catch (\Exception $ex) {}
            }

            if (!key_exists($dbField, $sortings)) {
                $sortings[$dbField] = 0;
            }

            if (!is_dir(storage_path('app/public/' . $dbField))) {
                Storage::makeDirectory('public/' . $dbField);
            }

            if (file_exists(storage_path('app/download/' . $filename))) {
                File::move(storage_path('app/download/' . $filename), $newFilePath);
            }

            if (file_exists($newFilePath)) {
                $info = pathinfo($newFilePath);
                $ext = $info['extension'];
                $data = getimagesize($newFilePath);
                $width = $data[0];
                $height = $data[1];
                $media = Media::updateOrCreate(
                    [
                        'path' => $publicPath
                    ], [
                        'title' => $filename,
                        'file_type' => $ext,
                        'file_width' => $width,
                        'file_height' => $height,
                        'file_size' => filesize($newFilePath),
                    ]
                );
                // create media reference to article
                $sortings[$dbField]++;
                MediaReference::updateOrCreate(
                    [
                        'entity_id' => $article->id,
                        'entity_type' => Article::class,
                        'media_id' => $media->id,
                        'field' => $dbField
                    ], [
                        'sorting' => $sortings[$dbField],
                    ]
                );
            }
        }
    }

    /**
     * create a category tree from given data
     * in this case the fields Export_XML2FMP::KEYWORD 1, Export_XML2FMP::KEYWORD 2 etc.
     *
     * @param Article $article
     * @param object $fieldData
     */
    protected function upsertCategory (Article $article, object $fieldData) {
        $categories = [];
        $categoryVariables = [
            'ABN_Artikelstamm_HPL_2020_20201201::PM0_Level4_Beschreibung',
            'ABN_Artikelstamm_HPL_2020_20201201::PM0_Level5_Beschreibung'
        ];
        foreach ($categoryVariables as $categoryVariable) {
            $categoryName = $fieldData->$categoryVariable;
            if (empty(trim($categoryName))) {
                break;
            }
            $categories[] = substr(str_replace('ABN ', '', $categoryName), 3);
        }

        foreach(self::upsertCategoryTree($categories) as $category) {
            ArticleCategory::updateOrCreate(
                [
                    'category_id' => $category->id,
                    'article_id' => $article->id
                ], []
            );
        }


    }

    /**
     * build a category tree from given array names
     * return a list of upsert categories
     * @param array $categories
     * @return array of Category
     */
    protected function upsertCategoryTree(array $categories) {

        $parentId = 0;
        $result = [];

        $parentCategorySlugs = [];

        foreach ($categories as $level => $categoryTitle) {

            $parentCategorySlugs[] = Str::slug($categoryTitle);
            $slug = implode('-', $parentCategorySlugs);
            $categoryModel = Category::updateOrCreate(
                [
                    'parent_id' => $parentId,
                    'title' => $categoryTitle
                ], [
                    'slug' => $slug
                ]
            );

            $parentId = $categoryModel->id;

            $result[] = $categoryModel;
        }



        return $result;
    }

    /**
     * @param object $fieldData
     * @return Article
     * @throws \Exception
     */
    protected function importArticleData($fieldData) {
        $titleVariable = "Export_XML2FMP::DESCRIPTION_SHORT";
        $priceVariable = "Export_XML2FMP::PRICE_AMOUNT";
        $modelVariable = "ABN_Artikelstamm_HPL_2020_20191202::Artikel_Beschreibung_2";
        $descriptionVariable = "Export_XML2FMP::DESCRIPTION_LONG";
        $shortDescriptionVariable = "Export_XML2FMP::DESCRIPTION_SHORT";
        $groupIdVariable = "Export_XML2FMP::REFERENCE_FEATURE_GROUP_ID";
        $groupNameVariable = "Export_XML2FMP::FGROUP";
        $keywords = [];

        $groupName = explode("\r", $fieldData->$groupNameVariable)[0];

        $articleGroup = ArticleGroup::updateOrCreate(
            [
                'key' => $fieldData->$groupIdVariable
            ],
            [
                'key' => self::getSlug($groupName),
                'title' => $groupName,
                'slug' => self::getSlug($groupName),
            ]
        );

        for ($i = 1; $i <= 8; $i++) {
            $keyword = $fieldData->{"Export_XML2FMP::KEYWORD " . $i};
            if (empty(trim($keyword))) {
                break;
            }
            $keywords[] = $keyword;
        }

        $shortDescriptionArr = [];
        foreach ((array) $fieldData as $key => $value) {

            if (strpos($key, '::Artikel_Beschreibung_1') > -1 && !empty($value)) {
                $shortDescriptionArr[] = $value;
            }
            if (strpos($key, '::Artikel_Beschreibung_2') > -1 && !empty($value)) {
                $shortDescriptionArr[] = $value;
            }
        }

        $vendorName = 'Export_XML2FMP::MANUFACTURER_NAME';

        $article = Article::updateOrCreate(
            ['id' => $fieldData->ID],
            [
                'article_number' => $fieldData->Artikelnummer,
                'group_id' => $articleGroup->id,
                'title' => $fieldData->$titleVariable,
                'vendor' => $fieldData->$vendorName,
                'ean' => $fieldData->EAN,
                'price' => (float) $fieldData->$priceVariable,
                'short_description' => implode('<br />', $shortDescriptionArr),
                'long_description' => $fieldData->$descriptionVariable,
                'keywords' => implode(' ', $keywords),
                'slug' => Str::slug($fieldData->Artikelnummer),
                'created_at' => new Carbon($fieldData->erstellt),
                'updated_at' => new Carbon($fieldData->geaendert),
                'transport' => $this->getTransportArray($fieldData),
                'online' => $fieldData->Online,
            ]
        );

        return $article;
    }

    protected function getTransportArray($fieldData) {
      $result = [];
      foreach ($fieldData as $key => $val) {
        if (strpos($key, 'Export_XML2FMP::UDX.EDXF.') !== false) {
          $key = str_replace('Export_XML2FMP::UDX.EDXF.', '', $key);
          $key = strtolower(str_replace(' ', '_', $key));
          $val = str_replace(",", ".", $val);

          if (in_array($key, ['volume_1', 'weight_1', 'length_1', 'width_1', 'depth_1', 'volume_2', 'weight_2', 'length_2', 'width_2', 'depth_2'])) {
            $val = (float) $val;
          }

          if (in_array($key, ['quantity_min_1', 'quantity_min_2', 'quantity_max_1', 'quantity_max_2'])) {
            $val = (int) $val;
          }

          if (in_array($key, ['length_1', 'width_1', 'depth_1', 'length_2', 'width_2', 'depth_2'])) {
            $val = $val * 1000;
          }

          $result[$key] = $val;
        }
      }
      return $result;
    }

    /**
     * @param Article $article
     * @param array $etimData
     */
    protected function importEtimData(Article $article, array $etimDatas) {
        $fKeyVariable = "ETIM_Daten::FNAME";
        $fDescVariable = "Features::FeatureDescription7_0";
        $fValueVariable = "ETIM_Daten::FVALUE";
        $fValueDescVariable = "FeatureValues::ValueDescription7_0";

        foreach ($etimDatas as $data) {
            $fKey = $data->$fKeyVariable;
            $fValue = $data->$fDescVariable;
            $vKey = $data->$fValueVariable;
            $vValue = $data->$fValueDescVariable;
            $isEtim = false;

            $featureValueValue = $vKey;
            $featureValueKey = $vKey;

            if (!empty($vKey) && substr($vKey, 0, 2) === 'EV') {
                $featureValueValue = $vValue;
                $isEtim = true;
            }

            $featureType = self::getValueType($featureValueValue);

            $feature = Feature::updateOrCreate(
                [
                    'key' => $fKey,
                ],
                [
                    'title' => $fValue,
                    'type' => $featureType,
                    'slug' => ImportArticleJob::getSlug($fValue),
                ]
            );

            $featureValue = FeatureValue::updateOrCreate(
                [
                    'parent' => $feature->id,
                    'key' => $featureValueKey,
                ],
                [
                    'title' => $isEtim ? $featureValueValue : $vKey,
                    'slug' => self::getSlug($isEtim ? $featureValueValue : $vKey),
                ]
            );

            ArticleFeature::updateOrCreate(
                [
                    'article_id' => $article->id,
                    'feature_id' => $feature->id,
                ],
                [
                    'feature_value' => $featureValue->id
                ]
            );
        }
    }

    protected function importFeatureById($id, $defaultValue, $defaultType) {
        $results = ImportService::getFeatureById($id);

        foreach ($results as $result) {
            $fieldData = $result->fieldData;

            $featureType = ImportArticleJob::getValueType($fieldData->ValueDescription7_0);
            $classKey = $fieldData->ClassID;

            $class = FeatureClass::updateOrCreate(
                [
                    'key' => ImportArticleJob::getSlug($classKey),
                ],
                [
                    'title' => $fieldData->FeatureDescription7_0,
                    'slug' => ImportArticleJob::getSlug($classKey),
                ]
            );

            $feature = Feature::updateOrCreate(
                [
                    'key' => $fieldData->FeatureID,
                ],
                [
                    'title' => $fieldData->FeatureDescription7_0,
                    'parent' => $class->id,
                    'type' => $featureType,
                    'slug' => ImportArticleJob::getSlug($fieldData->FeatureDescription7_0),
                ]
            );

            if (!empty(trim($fieldData->ValueID)) && !empty(trim($fieldData->ValueDescription7_0))) {
                $featureValue = FeatureValue::updateOrCreate(
                    [
                        'parent' => $feature->id,
                        'key' => $fieldData->ValueID,
                    ],
                    [
                        'title' => $fieldData->ValueDescription7_0,
                        'slug' => ImportArticleJob::getSlug($fieldData->ValueDescription7_0),
                    ]
                );
            }

            return $feature;
        }

    }

    public static function getSlug($value, $lang = 'de') {
        $type = self::getValueType($value);
        switch ($type) {
            case 'int':
                return (int) $value;
            case 'float':
                return str_replace('.', '_', (float) $value);
            case 'bool':
                return $value === 'true' ? 1 : 0;
            default:
                return Str::slug($value, $separator = '-', $lang);
        }

    }

    protected function importAdditionalData(Article $article, array $additionalData) {

        $featureMerge = [
            "Schrankmontage" => "WF000001",
            "TE" => "WF000002",
            "N_Klemmen" => "WF000003",
            "N_N_Klemmen" => "WF000004",
            "PE_Klemmen" => "WF000005",
            "PE_N_Klemmen" => "WF000006",
            "Einsatzbereich" => "WF000007",
            "Tueren" => "WF000008",
            "Tuermaterial" => "WF000009",
            "Tuerausfuehrung" => "WF000010",
            "Tueroeffnungsseite" => "WF000011",
            "Tueroeffnungswinkel" => "WF000012",
            "Montagehalterung" => "WF000013",
            "Normen" => "WF000014",
            "Bemessungsfrequenz" => "WF000015",
            "Umgebungstempratur_Mittelwert" => "WF000016",
            "Umgebungstemperatur_bei_Betrieb_Min" => "WF000017",
            "Umgebungstemperatur_bei_Betrieb_Max" => "WF000018",
            "EU_RoHS_Richtlinie" => "WF000024",
            "Bauhoehe" => "WF000025",
            "Feldbreite" => "WF000026",
            "Ausfuehrung_Messplatz" => "WF000027",
            "Ausfuehrung_Wandlerteil" => "WF000028",
            "Bemessungsstossspannungsfestigkeit" => "WF000029",
            "Bemessungsbetriebstrom" => "WF000030",
            "Bemessungsstossstromfestigkeit_Icc" => "WF000031",
            "Bemessungsstossstromfestigkeit_Ipk" => "WF000032",
            "Bemessungskurzzeitstromfestigkeit" => "WF000033",
            "RDF_Dauerbelastung" => "WF000034",
            "Verlustleistung" => "WF000035",
            "Feuerbestaendigkeit" => "WF000036",
            "Relative_Luftfeuchtigkeit_dauerhaft" => "WF000037",
            "Relative_Luftfeuchtigkeit_kurzzeitig" => "WF000038",
        ];

        foreach ($featureMerge as $featureProp => $featureVal) {
            if (empty(trim($additionalData[$featureProp]))) {
                continue;
            }

            $featureType = self::getValueType($additionalData[$featureProp]);

            $feature = Feature::updateOrCreate(
                [
                    'key' => $featureVal,
                ],
                [
                    'title' => $featureProp,
                    'type' => $featureType,
                    'slug' => ImportArticleJob::getSlug($featureProp),
                ]
            );

            $featureValue = FeatureValue::updateOrCreate(
                [
                    'parent' => $feature->id,
                    'key' => $additionalData[$featureProp],
                ],
                [
                    'title' => $additionalData[$featureProp],
                    'slug' => self::getSlug($additionalData[$featureProp])
                ]
            );

            ArticleFeature::updateOrCreate(
                [
                    'article_id' => $article->id,
                    'feature_id' => $feature->id,
                ],
                [
                    'feature_value' => $featureValue->id
                ]
            );
        }
    }

    public static function getValueType($value) {
        if (ImportService::isFloat($value)) {
            $value_type = 'float';
        } elseif (ImportService::isInt($value)) {
            $value_type = 'int';
        } elseif ($value === "false" || $value === "true") {
            $value_type = 'bool';
        } else {
            $value_type = 'string';
        }
        return $value_type;
    }

}
