<?php

namespace App\Jobs;

use App\Models\Article;
use App\Models\ArticleCategory;
use App\Models\ArticleFeature;
use App\Models\Category;
use App\Models\Feature;
use App\Models\FeatureValue;
use App\Models\Media;
use App\Models\MediaReference;
use App\Models\Product;
use App\Models\ProductArticle;
use App\Services\ImportService;
use App\Services\RequestService;
use GuzzleHttp\RequestOptions;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ImportProductJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * integer
     */
    private $productId = 0;

    /**
     * @var object
     */
    private $curlResult = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($productId)
    {
        $this->productId = $productId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $this->curlResult = $data = ImportService::getProductById($this->productId);
            $fieldData = $data->fieldData;
            $portalData = $data->portalData;

            $product = $this->upsertProductData($fieldData);
            $this->upsertProductArticleRelation($product, $portalData->n2n_Produkte_Artikel);

        } catch (\Exception $exception) {
            dd($exception);
        }
    }

    /**
     * @param object $fieldData
     * @return Product
     * @throws \Exception
     */
    protected function upsertProductData($fieldData) {

        $product = Product::where([
            ['title', '=', $fieldData->Bezeichnung_DE],
            ['vendor', '=', $fieldData->{'Artikel::Unternehmensgruppe'}],
        ])->first();

        if (is_null($product)) {
            $product = Product::updateOrCreate(
                [
                    'title' => $fieldData->Bezeichnung_DE,
                    'vendor' => $fieldData->{'Artikel::Unternehmensgruppe'}
                ],
                [
                    'id' => $fieldData->ID,
                    'description' => $fieldData->Beschreibung_DE,
                    'slug' => Str::slug($fieldData->Bezeichnung_DE),
                ]
            );
        }

        return $product;
    }

    /**
     * @param Product $product
     * @param $n2n
     */
    protected function upsertProductArticleRelation(Product $product, $n2n) {
        foreach ($n2n as $relation) {
            ProductArticle::updateOrCreate(
                [
                    'product_id' => $product->id,
                    'article_id' => $relation->{'n2n_Produkte_Artikel::ID_Artikel'}
                ], []
            );
        }
    }
}
