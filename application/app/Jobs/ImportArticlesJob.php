<?php

namespace App\Jobs;

use App\Services\ImportService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImportArticlesJob implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  /**
   * The number of times the job may be attempted.
   *
   * @var int
   */
  public $tries = 25;

  /**
   * The maximum number of exceptions to allow before failing.
   *
   * @var int
   */
  public $maxExceptions = 3;

  /**
   * The number of seconds the job can run before timing out.
   *
   * @var int
   */
  public $timeout = 60;

  /**
   * the offset of current request
   * @var int
   */
  protected $offset = 0;

  /**
   * @var int
   */
  protected $limit = 100;

  /**
   * Create a new job instance.
   *
   * @return void
   */
  public function __construct($offset = 0, $limit = 100)
  {
    $this->offset = $offset;
    $this->limit = $limit;
  }

  /**
   * Execute the job.
   *
   * @return void
   */
  public function handle()
  {
    $result = ImportService::getArticleList($this->offset, $this->limit);
    $count = count($result);
    if ($count > 0) {
      foreach ($result as $data) {
        ImportArticleJob::dispatch($data);
      }

      if ($count === $this->limit) {
        $offset = $this->offset + $this->limit;
        ImportArticlesJob::dispatch($offset, $this->limit)->delay(now()->addSeconds(30));
      }
    }

  }
}
