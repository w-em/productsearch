<?php

namespace App\Jobs;

use App\Models\Article;
use App\Services\ImportService;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImportUpdatedArticlesJob implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  /**
   * The number of times the job may be attempted.
   *
   * @var int
   */
  public $tries = 25;

  /**
   * The maximum number of exceptions to allow before failing.
   *
   * @var int
   */
  public $maxExceptions = 3;

  /**
   * The number of seconds the job can run before timing out.
   *
   * @var int
   */
  public $timeout = 60;

  /**
   * the offset of current request
   * @var int
   */
  protected $offset = 0;

  /**
   * @var int
   */
  protected $limit = 100;

  protected $date = null;

  /**
   * Create a new job instance.
   *
   * @return void
   */
  public function __construct($offset = 0, $limit = 100, $date = null)
  {
    $this->date = $date;
    $this->offset = $offset;
    $this->limit = $limit;
  }

  /**
   * Execute the job.
   *
   * @return void
   */
  public function handle()
  {
    if (is_null($this->date)) {
      $this->date = $this->getDate();
    }
    $result = ImportService::getUpdatedArticleList($this->offset, $this->limit, $this->date);
    $count = count($result);
    if ($count > 0) {
      foreach ($result as $data) {
        ImportArticleJob::dispatch($data);
      }

      if ($count === $this->limit) {
        $offset = $this->offset + $this->limit;
        ImportUpdatedArticlesJob::dispatch($offset, $this->limit, $this->date);
      }
    }

  }

  /**
   * get the last date of update
   * @return Carbon
   */
  public function getDate () {
    $article = Article::orderBy('updated_at', 'desc')->first();
    if ($article) {
      return $article->updated_at;
    }
    return Carbon::create(1970, 1, 1, 0, 0, 0);
  }
}
