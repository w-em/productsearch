<?php

namespace App\Jobs;

use App\Models\Feature;
use App\Models\FeatureClass;
use App\Models\FeatureValue;
use App\Services\ImportService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImportFeaturesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $offset = 0;

    protected $limit = 1000;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($offset = 0, $limit = 1000)
    {
        $this->offset = $offset;
        $this->limit = $limit;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $result = ImportService::getFeatureList($this->offset, $this->limit);
        $count = count($result);
        if ($count > 0) {
            foreach ($result as $data) {
                $this->importFeature($data);
            }

            if ($count === $this->limit) {
                $offset = $this->offset + $this->limit;
                ImportFeaturesJob::dispatch($offset, $this->limit);
            }
        }
    }

    protected function importFeature($data) {
        $fieldData = $data->fieldData;

        $featureType = ImportArticleJob::getValueType($fieldData->ValueDescription7_0);
        $featureKey = ImportArticleJob::getSlug($fieldData->FeatureDescription7_0);
        $classKey = $fieldData->ClassID;

        $class = FeatureClass::updateOrCreate(
            [
                'key' => $classKey,
            ],
            [
                'title' => $fieldData->FeatureDescription7_0,
                'slug' => ImportArticleJob::getSlug($fieldData->FeatureDescription7_0),
            ]
        );

        if (!empty(trim($fieldData->FeatureID)) && !empty(trim($fieldData->FeatureDescription7_0))) {
            $feature = Feature::updateOrCreate(
                [
                    'key' => $featureKey,
                    'etim_name' => $fieldData->FeatureID,
                ],
                [
                    'title' => $fieldData->FeatureDescription7_0,
                    'parent' => $class->id,
                    'type' => $featureType,
                    'slug' => ImportArticleJob::getSlug($fieldData->FeatureDescription7_0),
                ]
            );

            if (!empty(trim($fieldData->ValueID)) && !empty(trim($fieldData->ValueDescription7_0))) {
                $featureValue = FeatureValue::updateOrCreate(
                    [
                        'parent' => $feature->id,
                        'key' => $fieldData->ValueID,
                    ],
                    [
                        'title' => $fieldData->ValueDescription7_0,
                        'slug' => ImportArticleJob::getSlug($fieldData->ValueDescription7_0),
                    ]
                );
            }
        }

    }
}
