<?php

namespace App\Jobs;

use App\Models\Article;
use App\Models\ArticleCategory;
use App\Models\ArticleFeature;
use App\Models\Category;
use App\Models\Feature;
use App\Models\FeatureClass;
use App\Models\FeatureValue;
use App\Models\Media;
use App\Models\MediaReference;
use App\Services\ImportService;
use App\Services\RequestService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ImportFeatureJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 25;

    /**
     * The maximum number of exceptions to allow before failing.
     *
     * @var int
     */
    public $maxExceptions = 3;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 60;

    /**
     * @var array
     */
    private $curlResult = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($curlResult)
    {
        $this->curlResult = $curlResult;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $data = $this->curlResult;
        $fieldData = $data->fieldData;

        $featureType = ImportArticleJob::getValueType($fieldData->ValueDescription7_0);
        $featureKey = ImportArticleJob::getSlug($fieldData->FeatureDescription7_0);
        $classKey = $fieldData->ClassID;

        $class = FeatureClass::updateOrCreate(
            [
                'key' => ImportArticleJob::getSlug($classKey),
            ],
            [
                'title' => $fieldData->FeatureDescription7_0,
                'slug' => ImportArticleJob::getSlug($classKey),
            ]
        );

        $feature = Feature::updateOrCreate(
            [
                'key' => $featureKey,
                'etim_name' => $fieldData->FeatureID,
            ],
            [
                'title' => $fieldData->FeatureDescription7_0,
                'parent' => $class->id,
                'type' => $featureType,
                'slug' => ImportArticleJob::getSlug($fieldData->FeatureDescription7_0),
            ]
        );

        if (!empty(trim($fieldData->ValueID)) && !empty(trim($fieldData->ValueDescription7_0))) {
            $featureValue = FeatureValue::updateOrCreate(
                [
                    'parent' => $feature->id,
                    'key' => $fieldData->ValueID,
                ],
                [
                    'title' => $fieldData->ValueDescription7_0,
                    'slug' => ImportArticleJob::getSlug($fieldData->ValueDescription7_0),
                ]
            );
        }
    }


}
