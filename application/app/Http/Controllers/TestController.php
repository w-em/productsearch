<?php

namespace App\Http\Controllers;

use App\Jobs\ImportArticleJob;
use App\Jobs\ImportUpdatedArticlesJob;
use App\Models\ArticleCategory;
use App\Models\Category;
use App\Services\ImportService;
use App\Services\SearchService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;


class TestController extends Controller
{
  public function __construct()
  {
  }

  function index(Request $request)
  {
    //$this->searchCategory();
    //dd(Article::with(['features'])->findOrFail(10135)->toArray());
    // $this->importDatenblatt();
   // $this->importArticleById();
    // dd(Article::with(['features'])->findOrFail(32)->toArray());
    //dd(ImportService::getArticleList());
    dump(request()->getHttpHost());
    dump(parse_url(Config::get('app.stage_url')));
    dd(parse_url(Config::get('app.stage_url'))['host']);
    // dd(request()->getHost() === Config::get('app.stage_url'));
    // ImportUpdatedArticlesJob::dispatchSync();
    // $this->search();

    //$this->getSuggestions();
  }

  private function getSuggestions () {
    dd(SearchService::getSuggestions('amig'));
  }

  private function searchCategory()
  {
    $term = 'amigo';
    $results = [];
    // ['installationskleinverteiler']
    $articleCategoryResults = SearchService::getSearchResult($term, $selectedCategories = [], [])->get()->pluck('id');
    $categoriesUids = ArticleCategory::whereIn('id', $articleCategoryResults)->groupBy('category_id')->get()->pluck('category_id');

    foreach ($categoriesUids as $categoryUid) {
      $cat = Category::findOrFail($categoryUid);
      if ($cat->parent_id === 0) {
        $results[$cat->id] = $cat->toArray();
      }
    }

    foreach ($categoriesUids as $categoryUid) {
      $cat = Category::findOrFail($categoryUid);
      if ($cat->parent_id === 0) {
        continue;
      }

      if (!key_exists($cat->parent_id, $results)) {
        $parentCat = Category::findOrFail($cat->parent_id);
        $results[$parentCat->id] = $parentCat->toArray();
      }

      $results[$cat->parent_id]['childs'][] = $cat->toArray();

    }
    dd($results);
  }

  private function importFeatures()
  {
    /*
    $features = ImportService::getFeatureList(0, 100);

    //$feature = $features[0];

    ImportFeatureJob::dispatchSync($feature);*/
    // ImportFeaturesJob::dispatchSync(0, 1000);

  }

  private function importDatenblatt()
  {
    // Opening the file for reading...
    $fp = fopen(__DIR__ . '/datenblatt_aufbau.csv', 'r');
    $results = [];
    // Headrow
    $head = fgetcsv($fp, 4096, ';', '"');

    // Rows
    while ($column = fgetcsv($fp, 4096, ';', '"')) {
      // This is a great trick, to get an associative row by combining the headrow with the content-rows.
      $column = array_combine($head, $column);

      $results[] = $column;
    }

    $print = [];

    foreach ($results as $result) {
      $arrayKeys = array_keys($result);
      if ($result[$arrayKeys[0]] === 'Umgebungsbedingungen') {
        $print[] = $result;
      }
    }

    foreach ($print as $p) {
      echo "'" . $p['Field'] . "'," . "// " . $p["Bezeichnung"] . "\n";
    }

    exit;
  }

  private function search()
  {
    // $this->importArticleById();
    $term = '';
    $term1 = '';
    $filters = [[
      "feature_id" => 1,
      "feature_slug" => "montageart",
      "values" => [
        1
      ]
    ]];

    $limit = 24;
    $offset = 0;
    // ['installationskleinverteiler']
    $result = SearchService::search($term, $selectedCategories = ['installationskleinverteiler'], $filters);
    dump($result);
  }

  private function importArticleById()
  {
    // $data = ImportService::getArticleById(10135, 'web1-Artikel_DE');
    $data = ImportService::getArticleById(32, 'web1-Artikel_DE');
    // $data = ImportService::getArticleById(912, 'web1-Artikel_DE');
    ImportArticleJob::dispatchSync($data);
  }

  private function importArticles()
  {
    $offset = 10;
    $limit = 100;
    $result = ImportService::getArticleList($offset, $limit);

    foreach ($result as $data) {
      ImportArticleJob::dispatchSync($data);
    }
  }
}
