<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DatasheetController extends Controller
{
  const FOOTER_LOGO_IMAGE = 'images/Brand_Logo_RGB_LifeGreen.png';
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('guest');
  }

  /**
   * @param Request $request
   * @param $id
   * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
   * @throws \Exception
   */
  protected function getById(Request $request, $id)
  {
    $download = false;

    $article = Article::with(['features', 'products'])->where([['slug', '=', $id]])->first();
    if (!$article) {
      throw new NotFoundHttpException();
    }

    $images = [];

    if (count($article->medias) > 0) {
      foreach ($article->medias as $image) {
        $images[] = [
          "path" => public_path('/storage/' . $image['path']),
          "type" => $image['type']
        ];
      }
    }

    $data = [
      'fontPath' => asset('storage/fonts/roboto-condensed-v19-latin/roboto-condensed-v19-latin-300.ttf'),
      'article' => $article,
      'images' => $images,
      'inlineCss' => file_get_contents(public_path('css/pdf.css')),
      'logoImage' => public_path(self::FOOTER_LOGO_IMAGE)
    ];

    $pdf = \App::make('dompdf.wrapper');
    $pdf->loadView('download.datasheet', $data);
    if ($download === true) {
      return $pdf->download($id . '.pdf');
    } else {
      return $pdf->stream();
    }
    /*
    $article = Article::with(['features', 'products'])->where([
      ['slug', '=', $id]
    ])->first();

    if ($article) {
      $html = $this->getArticlePdfContent($article);

      $pdf = \App::make('dompdf.wrapper');
      $pdf->loadHTML($html);

      if ($download === true) {
        return $pdf->download($id . '.pdf');
      } else {
        return $pdf->stream();
      }
    } else {
      throw new NotFoundHttpException();
    }*/
  }

  /**
   * @param Article $article
   * @return array|string
   * @throws \Throwable
   */
  protected function getArticlePdfContent(Article $article)
  {

    $images = [];

    if (count($article->medias) > 0) {
      foreach ($article->medias as $image) {
        $images[] = [
          "path" => public_path('/storage/' . $image['path']),
          "type" => $image['type']
        ];
      }
    }

    $data = [
      'article' => $article,
      'images' => $images,
      'inlineCss' => file_get_contents(public_path('css/pdf.css')),
      'logoImage' => public_path(self::FOOTER_LOGO_IMAGE)
    ];

    return view('download.article', $data)->render();
  }

  /**
   * @param Request $request
   */
  protected function multiple(Request $request)
  {
    $slugs = $request->input('id', []);

    $articles = [];

    foreach ($slugs as $slug) {
      $article = Article::with(['features', 'products'])->where([['slug', '=', $slug]])->first();
      if (!$article) {
        continue;
      }

      $images = [];

      if (count($article->medias) > 0) {
        foreach ($article->medias as $image) {
          $images[] = [
            "path" => public_path('/storage/' . $image['path']),
            "type" => $image['type']
          ];
        }
      }

      $articles[] = [
        'article' => $article,
        'images' => $images
      ];
    }

    $data = [
      'fontPath' => asset('storage/fonts/roboto-condensed-v19-latin/roboto-condensed-v19-latin-300.ttf'),
      'articles' => $articles,
      'inlineCss' => file_get_contents(public_path('css/pdf.css')),
      'logoImage' => public_path(self::FOOTER_LOGO_IMAGE)
    ];

    // dd(storage_path('fonts/roboto-condensed-normal_05f82b1d798690119a8263c1eae2bd7e.ttf'));
    // return view('download.datasheets', $data)->render();

    $pdf = \App::make('dompdf.wrapper');
    $pdf->loadView('download.datasheets', $data);

    return $pdf->stream();
  }
}
