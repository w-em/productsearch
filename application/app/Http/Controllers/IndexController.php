<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\ArticleCategory;
use App\Models\Category;
use App\Models\Feature;
use App\Models\FeatureValue;
use App\Services\SearchService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class IndexController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('guest');
  }

  public function indexAction(Request $request)
  {

    $secure = $request->isSecure();
    $host = parse_url(url(''));
    $isStage = parse_url(Config::get('app.stage_url'))['host'] === request()->getHttpHost();
    $result = [
      'host' => $host,
      'stage' => $isStage,
      'secure' => $secure
    ];

    return view('index', $result);

  }

  protected function getById(Request $request, $id)
  {
    $article = Article::with(['features', 'categories'])->where([
      ['slug', '=', $id]
    ])->first();
    if ($article) {
      return $article->makeVisible([
        'long_description',
        'medias',
        'slug',
        'categories',
        'properties',
        'features',
        'transport',
      ]);
    } else {
      throw new NotFoundHttpException();
    }
  }

  /**
   * @param Request $request
   */
  protected function search(Request $request)
  {

    $limit = (int)$request->get('limit', 24);
    $term = $request->get('term', '');

    $page = null;

    if (!in_array($limit, [24, 48])) {
      $limit = 24;
    }

    $selectedCategories = $request->get('categories', []);
    $baseCategory = $request->get('base', null);
    if ($baseCategory) {
      $selectedCategories[] = $baseCategory;
    }

    $filters = $this->getFilterByRequest($request);

    return SearchService::search($term, $selectedCategories, $filters, $page, $limit);
  }

  /**
   * @param Request $request
   * @return array
   * TODO: Category filter by term search
   */
  protected function getCategories(Request $request)
  {
    $term = $request->get('term', '');
    $results = [];
    $articleCategoryResults = SearchService::getSearchResult($term, [], [])->get()->pluck('id');

    $categoriesUids = ArticleCategory::whereIn('article_id', $articleCategoryResults)->groupBy('category_id')->get()->pluck('category_id');
    foreach ($categoriesUids as $categoryUid) {
      $cat = Category::findOrFail($categoryUid);
      if ($cat->parent_id === 0) {
        $results[$cat->id] = $cat->toArray();
      }
    }

    foreach ($categoriesUids as $categoryUid) {
      $cat = Category::findOrFail($categoryUid);
      if ($cat->parent_id === 0) {
        continue;
      }

      if (!key_exists($cat->parent_id, $results)) {
        $parentCat = Category::findOrFail($cat->parent_id);
        $results[$parentCat->id] = $parentCat->toArray();
      }

      $results[$cat->parent_id]['childs'][] = $cat->toArray();
    }

    return [
      'categories' => array_values($results)
    ];
  }

  /**
   * @param Request $request
   * @return array
   */
  protected function getBaseCategories(Request $request)
  {
    $categories = Category::where([
      ['parent_id', '=', 0]
    ])->get()->makeHidden(['parent_id']);

    return [
      "basecategories" => $categories
    ];
  }

  /**
   * @param Request $request
   * @return array
   */
  protected function getFilters(Request $request)
  {

    $term = $request->get('term', '');
    $categoriesRequest = $request->get('categories', []);
    $baseCategory = $request->get('base', null);
    if ($baseCategory) {
      $categoriesRequest[] = $baseCategory;
    }

    $selectedCategories = [];
    foreach ($categoriesRequest as $categorySlug) {
      $selectedCategories[] = $categorySlug;
    }

    $filters = $this->getFilterByRequest($request);

    return [
      "filters" => SearchService::getFilters($term, $filters, $selectedCategories)
    ];

  }

  /**
   * @param Request $request
   * @return array
   */
  protected function getFilterByRequest(Request $request)
  {
    $results = [];

    $filtersParam = $request->get('filters');
    if (!$filtersParam) {
      return $results;
    }

    $filters = json_decode($filtersParam);
    if (!$filters) { // not a json
      return $results;
    }

    foreach ($filters as $filter) {
      $slug = array_shift($filter);

      $feature = Feature::where([['slug', '=', $slug]])->first();
      $featureValues = FeatureValue::selectRaw(DB::raw('group_concat(distinct(id)) as val'))->where([
        ['parent', '=', $feature->id],
      ])->whereIn('slug', $filter)->first();

      if ($featureValues) {
        $featureValues = array_map('intval', explode(',', $featureValues->val));
      }

      if ($feature && count($featureValues)) {
        $results[] = [
          'feature_id' => $feature->id,
          'feature_slug' => $feature->slug,
          'values' => $featureValues
        ];
      }
    }
    return $results;

  }

  protected function getSuggestion(Request $request)
  {
    $term = $request->get('term', '');
    return SearchService::getSuggestions($term);
  }
}
