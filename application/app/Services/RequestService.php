<?php

namespace App\Services;

use GuzzleHttp\Psr7\CachingStream;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Stream;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;

class RequestService {

    const USERNAME = "wolter_n";
    const PASSWORD = "secret";

    /**
     * return the curl session cache identifier
     * @return string
     */
    public static function getCurlSessionIdentifier() {
        $cacheIdentifier = sha1('curl-sesion' . self::USERNAME);
        return $cacheIdentifier;
    }

    /**
     * get the base url
     * @param string $layout
     * @param string $database
     * @return string
     */
    public static function getBaseUrl ($layout = 'web1-Artikel_DE', $database = 'abn') {
        return 'https://dbserv.ddns.net/fmi/data/v1/databases/'. $database . '/layouts/' . $layout;
    }

    /**
     * call a url with given parameters
     * @param $url
     * @param string $type
     * @param array $parameters
     * @return bool
     */
    public function callUrl($url, $type = 'GET', $parameters = []) {
        // Initiate the Request Factory, which allows to run multiple requests
        /** @var \TYPO3\CMS\Core\Http\RequestFactory $requestFactory */
        $requestFactory = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Http\RequestFactory::class);

        $response = $requestFactory->request($url, $type, $params);
        // Get the content as a string on a successful request
        if ($response->getStatusCode() === 200) {
            $content = $response->getBody()->getContents();
            return $content;
        }

        return false;
    }

    /**
     * get the session token from filemaker server
     * @return bool|mixed
     */
    public static function getSessionToken () {
        $cacheIdentifier = self::getCurlSessionIdentifier();
        $entry = Cache::get($cacheIdentifier);
        if (!$entry) {
            return self::createSession();
        }

        return $entry;
    }

    /**
     * create a session to database server
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected static function createSession () {
        $url = 'https://dbserv.ddns.net/fmi/data/vLatest/databases/schneider/sessions';

        $client = new \GuzzleHttp\Client();
        $additionalOptions = [
            'headers' => [
                'Cache-Control' => 'no-cache',
                'Authorization' => 'Basic d29sdGVyX246YWNjZXNz',
                'Content-Type' => 'application/json',
            ],
            'allow_redirects' => false
        ];
        $response = $client->request('POST', $url, $additionalOptions);

        if ($response->getStatusCode() === 200) {
            $content = json_decode($response->getBody()->getContents());

            Cache::put(self::getCurlSessionIdentifier(), $content->response->token, 120);

            return $content->response->token;
        }

        return false;
    }

    /**
     * get an authenticated Guzzle Client
     * @return \GuzzleHttp\Client
     */
    public static function getAuthenticatedClient() {
        $token = self::getSessionToken();

        $options = [
            // Additional headers for this specific request
            'headers' => [
                'Cache-Control' => 'no-cache',
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $token,
            ],
            'allow_redirects' => false
        ];

        return new \GuzzleHttp\Client($options);
    }

    /**
     * @param $url
     * @param array $body
     * @return array|object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function post($url, array $body = []) {
        $token = self::getSessionToken();

        $options = [
            // Additional headers for this specific request
            'headers' => [
                'Cache-Control' => 'no-cache',
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $token,
            ],
            'allow_redirects' => false,
            'json' => $body
        ];

        $client = new \GuzzleHttp\Client($options);

        $response = $client->request('POST', $url, $options);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * @param $url
     * @param array $body
     * @return array|object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function get($url) {
        $token = self::getSessionToken();

        $options = [
            // Additional headers for this specific request
            'headers' => [
                'Cache-Control' => 'no-cache',
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $token,
            ],
            'allow_redirects' => false,
        ];

        $client = new \GuzzleHttp\Client($options);
        $response = $client->request('GET', $url, $options);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * @param $url
     * @return \Psr\Http\Message\ResponseInterface
     */
    public static function downloadFile($url) {
        $token = RequestService::getSessionToken();

        $pathInfo = pathinfo($url);
        $fileName = explode('?', $pathInfo['basename'])[0];

        $downloadPath = 'app/download';
        $storagePath = storage_path($downloadPath);

        if (!is_dir($storagePath)) {
            Storage::makeDirectory('download');
        }

        $file = $storagePath . "/" . $fileName;
        $fileHandle = fopen($file, "w+");

        $client = new \GuzzleHttp\Client();
        return $client->get($url, [
            RequestOptions::SINK => $fileHandle,
            RequestOptions::HEADERS => [
                'Cache-Control' => 'no-cache',
                'Authorization' => 'Bearer ' . $token,
            ],
        ]);
    }
}
