<?php

namespace App\Services;

use App\Models\Feature;

class FieldService {

    public const ARTICLE_HEAD_DATA = [
        'EF000008',// Breite [mm]
        'EF000040',// Höhe [mm]
        'EF000049',// Tiefe [mm]
        'EF000332',// Bauhöhe
        // '',// Feldbreite
        'EF000266',// Anzahl Reihen
        'EF002950',// Anzahl Teilungseinheiten
        // '',// Ausführung Messplatz
        // '',// Ausführung Wandlerteil
        'EF000584',// Anzahl der N-Klemmen
        // '',// Anzahl der N/N-Klemmen
        // '',// Anzahl der PE-Klemmen
        // '',// Anzahl der PE/N-Klemmen
        // '',// Einsatzbereich
        'EF000007',// Farbe
        'EF000116',// RAL-Ton
    ];

    public const ARTICLE_ADDITIONAL_DATA = [
        'EF000003',// Schrankmontage
        'EF000325',// Kabeleinführung
        'EF000122',// Anzahl Türen
        // '',// Türmaterial
        // '',// Türausführung
        // '',// Türöffnungsseite
        'EF002642',// Türöffnungswinkel [°]
        'EF000123',// Anzahl Schlösser
        // '',// Montagehalterung
        'EF001596',// Gehäusematerial
        // '',// Normen
        // '',// Langtext

    ];

    public const ARTICLE_ELECTRIC_DATA = [
        'EF000228',// Bemessungsspannung AC Un [V]
        'EF002578',// Bemessungsstoßspannungsfestigkeit Uimp [kV]
        'EF000001',// Nennstrom In [A]
        'EF000227',// Bemessungsbetriebstrom Inc [A]
        // '',// Bemessungsstoßstromfestigkeit Icc [A]
        // '',// Bemessungsstoßsstromfestigkeit Ipk [kA]
        'EF007050',// Bemessungskurzzeitstromfestigkeit Icw (1s) [kA]
        'EF000034',// Bemessungsfrequenz fn [Hz]
        // '',// RDF bei Dauerbelastung
        // '',// Verlustleistung [W]
        'EF005474',// IP-Schutzart
        'EF002569',// Überspannungskategorie
        'EF002570',// Verschmutzungsgrad nach IEC 61010-1
    ];

    public const ARTICLE_AMBIENT_DATA = [
        'EF007515',// Umgebungstempratur Mittelwert [°C]
        // '',// Umgebungstemperatur bei Betrieb min. [°C]
        // '',// Umgebungstemperatur bei Betrieb max. [°C]
        // '',// Feuer Beständigkeit [°C]
        // '',// Relative Luftfeuchtigkeit dauerhaft [%]
        // '',// Relative Luftfeuchtigkeit kurzzeitig [%]
    ];

    public static function getHeadData () {

        return self::ARTICLE_HEAD_DATA;
        $results = [];
        foreach (self::ARTICLE_HEAD_DATA as $featureKey) {
            $first = Feature::where([['name', '=', $featureKey]])->first();
            if ($first) {
                $results[$featureKey] = [
                    'name' => $first,
                    'value' => '-'
                ];
            }
        }
    }

    public static function getAdditionalData () {
        return self::ARTICLE_ADDITIONAL_DATA;
    }

    public static function getElectricData () {
        return self::ARTICLE_ELECTRIC_DATA;
    }

    public static function getAmbientData () {
        return self::ARTICLE_AMBIENT_DATA;
    }
}
