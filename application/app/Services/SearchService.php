<?php

namespace App\Services;

use App\Models\Article;
use App\Models\ArticleFeature;
use App\Models\Category;
use App\Models\Feature;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use TeamTNT\TNTSearch\Indexer\TNTIndexer;
use TeamTNT\TNTSearch\TNTSearch;

class SearchService
{
  /**
   * @param string $term
   * @param array $selectedCategories
   * @param array $selectedFilters
   * @return Article
   */
  public static function getSearchResult($term = '', $selectedCategories = [], $selectedFilters = [])
  {
    $query = new Article;
    $articleUids = [];

    // filter out categorues to which the given article is assigned
    if (count($selectedCategories) > 0) {
      $categoryIds = Category::whereIn('slug', $selectedCategories)->get()->pluck('id');
      $query = $query->whereIn('id', function($query) use ($categoryIds) {
        $query->select('article_id')->from('article_categories')->where('category_id', $categoryIds);
      });
    }

    // filter out filters to which the given article is assigned
    if (count($selectedFilters) > 0) {
      foreach ($selectedFilters as $selectedFilter) {
        $sql = "select group_concat(distinct a.id) as article_ids
                  from articles a
                  left join article_features as af on (af.article_id = a.id)
                  where
                      (af.feature_id = " . (int)$selectedFilter['feature_id'] . "
                      and af.feature_value IN (\"" . implode('","', $selectedFilter['values']) . "\")) ";

        $result = DB::select($sql, []);

        if (count($result) > 0) {
          if (!empty(trim($result[0]->article_ids))) {
            $articleUids = explode(',', $result[0]->article_ids);
          } else {
            $articleUids = [];
          }
        } else {
          $articleUids = [];
        }
      }

      $query = $query->whereIn('id', $articleUids);
    }

    if (!empty(trim($term))) {

      $constraints = $query; // not necessary but for better readability
      $query = Article::search($term)->constrain($constraints);
    }

    return $query;
  }

  /**
   * @param string $term
   * @param array $selectedFilters
   * @param array $selectedCategories
   * @return array
   */
  public static function getFilters($term = '', $selectedFilters = [], $selectedCategories = [])
  {
    $result = [];
    $subSearchFilters = [];
    $secondFilters = [];
    $tempSlugs = [];

    foreach ($selectedFilters as $filterKey => $basefilter) {
      $secondArticleUids = self::getSearchResult($term, $selectedCategories, $secondFilters)->get()->pluck('id');
      $filters = self::getFiltersFromResult($secondArticleUids);
      foreach ($filters as $k => $filter) {
        if ($filter['slug'] === $basefilter['feature_slug']) {
          $subSearchFilters[] = $filter;
          $tempSlugs[] = $basefilter['feature_slug'];
        }
      }
      $secondFilters[$filterKey] = $basefilter;
    }

    $articleUidsForFilteredSearch = self::getSearchResult($term, $selectedCategories, $selectedFilters)->get()->pluck('id');

    $filters = self::getFiltersFromResult($articleUidsForFilteredSearch);

    foreach ($filters as $filter) {
      if (!in_array($filter['slug'], $tempSlugs)) {
        $subSearchFilters[] = $filter;
      }
    }

    return $subSearchFilters;
  }

  /**
   * @param string $term
   * @param array $selectedFilters
   * @param array $selectedCategories
   * @param null $page
   * @param int $limit
   * @return array
   */
  public static function search($term = '', $selectedCategories = [], $selectedFilters = [], $page = null, $limit = 24)
  {

    $article = self::getSearchResult($term, $selectedCategories, $selectedFilters);

    $article->orderBy('updated_at', 'asc');

    $paginator = self::paginate($article->get(), $limit, $page);

    return [
      'items' => array_values($paginator->items()),
      'total' => (int)$paginator->total(),
      'limit' => (int)$paginator->perPage(),
    ];
  }

  /**
   * @param Collection $articleResult
   * @return array
   */
  public static function getFiltersFromResult($articleUids, $featureUids = [])
  {

    $allowedFeatureUids = Feature::whereIn('key', [
      'EF000003', 'EF000008', 'EF000040', 'EF000049', 'EF000266'
    ])->get()->pluck('id');

    $articleFeaturesQuery = ArticleFeature::select('*')
      ->selectRaw('count(*) as total')
      ->whereIn('article_id', $articleUids)
      ->whereIn('feature_id', $allowedFeatureUids)
      ->groupBy('feature_id', 'feature_value');

    $articleFeatures = $articleFeaturesQuery->get();

    $filters = [];

    foreach ($articleFeatures as $articleFeature) {
      $value = $articleFeature->value;
      $total = $articleFeature->total;

      if (is_bool($value)) {
        $value = $value ? 'true' : 'false';
      }

      $feature = $articleFeature->feature;

      if (!key_exists($articleFeature->feature_id, $filters)) {
        $filters[$articleFeature->feature_id] = [
          'label' => $feature->title,
          'slug' => $feature->slug,
          'options' => []
        ];
      }

      if (!key_exists($value, $filters[$articleFeature->feature_id]['options'])) {

        if ($value !== '-') {
          $filters[$articleFeature->feature_id]['options'][$value] = [
            'label' => $value,
            'slug' => Str::slug($value),
            'count' => $total
          ];
        }
      }
    }

    $results = [];
    foreach ($filters as $filter) {
      $options = array_values($filter['options']);
      $result = $filter;
      usort($options, function ($a, $b) {
        return $a['label'] <=> $b['label'];
      });
      $result['options'] = $options;
      if (count($options) > 0) {
        $results[] = $result;
      }

    }

    return $results;
  }

  /**
   * @param string $term
   * @throws \TeamTNT\TNTSearch\Exceptions\IndexNotFoundException
   */
  public static function getSuggestions($term = '') {

    $articles = new Collection();
    foreach (Article::TRIGRAMS_FIELDS as $fieldName) {
      $datas = self::getSuggestionsFor($fieldName, $term);
      foreach ($datas as $data) {
        $articles->add($data);
      }
    }

    $sorted = $articles->sort(function ($a, $b) {
      return $a->distance < $b->distance ? -1 : 1;
    });

    $results = [];
    $i = 0;
    foreach ($sorted as $result) {
      $fieldName = $result->field;
      $results[] = [
        'suggestion' => $result->$fieldName,
        'field' => $fieldName,
        'value' => strip_tags(str_replace('<br />', ' ', $result->$fieldName))
      ];
      $i++;
      if ($i === 10) {
        break;
      }
    }

    return $results;
  }

  /**
   * @param $fieldName
   * @param $term
   * @return mixed
   * @throws \TeamTNT\TNTSearch\Exceptions\IndexNotFoundException
   */
  public static function getSuggestionsFor($fieldName, $term) {
    $TNTIndexer = new TNTIndexer;
    $trigrams   = $TNTIndexer->buildTrigrams($term);

    $tnt = new TNTSearch;

    $driver = config('database.default');
    $config = config('scout.tntsearch') + config("database.connections.$driver");

    $tnt->loadConfig($config);
    $tnt->setDatabaseHandle(app('db')->connection()->getPdo());

    $tnt->selectIndex('article_ngrams_' . $fieldName . '.index');
    $res  = $tnt->search($trigrams, 10);
    $keys = collect($res['ids'])->values()->all();

    $suggestions = Article::whereIn('id', $keys)->get();

    $priority = [
      'short_description' => 10
    ];

    $suggestions->map(function ($article) use ($term, $fieldName, $priority) {
      $term = strtolower(strip_tags(trim($term)));
      $fieldValue = strtolower(strip_tags(trim($article->$fieldName)));

      if (strpos($fieldValue, ' ') > 0) {
        $ex = explode(' ', $fieldValue);
        $len = count($ex);
        $distance = 0;
        foreach ($ex as $val) {
          $distance = $distance + levenshtein($term, $val);
        }
        $article->distance = $distance / $len;

        if (key_exists($fieldName, $priority)) {
          $article->distance = $article->distance - $priority[$fieldName];
        }
      }
      else {
        $article->distance = levenshtein($term, $fieldValue);
      }

      $article->field = $fieldName;
    });

    return $suggestions->values()->all();
  }

  /**
   * creates a pagination of collection of items
   *
   * @param array|Collection $items
   * @param int $perPage
   * @param int $page
   * @param array $options
   *
   * @return LengthAwarePaginator
   */
  public static function paginate($items, $perPage = 12, $page = null, $options = [])
  {
    $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

    $items = $items instanceof Collection ? $items : Collection::make($items);

    return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
  }
}
