<?php

namespace App\Services;

use App\Jobs\ImportArticlesJob;
use App\Jobs\ImportFeaturesJob;
use App\Jobs\ImportUpdatedArticlesJob;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ImportService
{

  static function startArticleImport()
  {
    $offset = 0;
    $limit = 100;
    ImportArticlesJob::dispatch($offset, $limit);
  }

  /**
   * search for updated articles
   */
  static function startUpdatedArticleImport() {
    $offset = 0;
    $limit = 100;
    ImportUpdatedArticlesJob::dispatch($offset, $limit);
  }

  /**
   * start the feature import
   */
  static function startFeatureImport()
  {
    $offset = 0;
    $limit = 500;
    ImportFeaturesJob::dispatch($offset, $limit);
  }

  static function getEtimDatas($offset = 0, $limit = 10, $layout = 'ETIM_Daten')
  {

    $params = http_build_query([
      '_offset' => 10,
      '_limit' => 1
    ]);

    $url = RequestService::getBaseUrl($layout) . '/records?' . $params;
    $content = RequestService::get($url);

    if ($content) {
      $parsedContent = $content;
      return $parsedContent->response->data;
    }
    return [];
  }

  /**
   * @param int $offset
   * @param int $limit
   * @param Carbon $date
   * @param string $layout
   * @return array
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  static function getUpdatedArticleList($offset = 0, $limit = 100, Carbon $date, $layout = 'web1-Artikel_DE')
  {
    $url = RequestService::getBaseUrl($layout) . '/_find';
    $listOfAllowedCategories = ['Installationskleinverteiler', 'Zählerschränke', 'Feldverteiler'];
    $results = [];

    $query = [
      "query" => [],
      "limit" => $limit
    ];

    foreach ($listOfAllowedCategories as $cat) {
      $query["query"][] = [
        "geaendert" => ">" . $date->format('m/d/Y H:i:s'),
        "Unternehmensgruppe" => "=ABN",
        "Export_XML2FMP::KEYWORD 1" => "*",
        "ABN_Artikelstamm_HPL_2020_20201201::PM0_Level4_Beschreibung" => "=ABN " . $cat
      ];
    }

    if ($offset > 0) {
      $query["offset"] = $offset;
    }

    try {
      $content = RequestService::post($url, $query);
      if ($content) {
        $parsedContent = $content;
        return $parsedContent->response->data;
      }
    } catch (\Exception $ex) {}

    return $results;
  }

  /**
   * @param int $offset
   * @param int $limit
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  static function getArticleList($offset = 0, $limit = 10, $layout = 'web1-Artikel_DE')
  {
    $url = RequestService::getBaseUrl($layout) . '/_find';
    $query = [
      "query" => [
        [
          "Unternehmensgruppe" => "=ABN",
          "Export_XML2FMP::KEYWORD 1" => "*",
          "ABN_Artikelstamm_HPL_2020_20201201::PM0_Level4_Beschreibung" => "*"
        ]
      ],
      "limit" => $limit
    ];

    if ($offset > 0) {
      $query["offset"] = $offset;
    }

    $content = RequestService::post($url, $query);

    if ($content) {
      $parsedContent = $content;
      return $parsedContent->response->data;
    }
    return [];
  }

  /**
   * @param $id
   * @param string $layout
   * @return mixed
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  static function getArticleById($id, $layout = 'web1-Artikel_DE')
  {
    $url = RequestService::getBaseUrl($layout) . '/records/' . $id;
    $content = RequestService::get($url);

    if ($content) {
      $parsedContent = $content;
      $articleResult = $parsedContent->response->data;

      if (count($articleResult) > 0) {
        return $articleResult[0];
      }

      throw new ModelNotFoundException('article (id: ' . $id . ') not found');
    }
  }

  /**
   * @param $id
   * @param string $layout
   * @return mixed
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  static function getProductById($id, $layout = 'web1-Produkte_DE')
  {
    $url = RequestService::getBaseUrl($layout) . '/records/' . $id;
    $content = RequestService::get($url);

    if ($content) {
      $parsedContent = $content;
      $articleResult = $parsedContent->response->data;

      if (count($articleResult) > 0) {
        return $articleResult[0];
      }

      throw new ModelNotFoundException('product (id: ' . $id . ') not found');
    }
  }

  static function getFeatureById($featureId)
  {
    $layout = 'Features';
    $url = RequestService::getBaseUrl($layout) . '/_find';
    $query = [
      "query" => [
        [
          "FeatureID" => $featureId,
          "FeatureDescription7_0" => '*'
        ]
      ],
      "limit" => 1
    ];

    $content = RequestService::post($url, $query);

    if ($content) {
      $parsedContent = $content;
      return $parsedContent->response->data;
    }
    return [];
  }

  static function getFeatureList($offset = 0, $limit = 100)
  {
    $layout = 'Features';
    $url = RequestService::getBaseUrl($layout) . '/_find';
    $query = [
      "query" => [
        [
          "FeatureID" => "*",
        ]
      ],
      "limit" => $limit
    ];

    if ($offset > 0) {
      $query["offset"] = $offset;
    }

    $content = RequestService::post($url, $query);

    if ($content) {
      $parsedContent = $content;
      return $parsedContent->response->data;
    }
    return [];
  }

  public static function isFloat($num)
  {
    return is_float($num) || is_numeric($num) && ((float)$num != (int)$num);
  }

  public static function isInt($num)
  {
    return ctype_digit($num);
  }
}
