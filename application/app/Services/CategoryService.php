<?php

namespace App\Services;

use App\Jobs\ImportArticleJob;
use App\Jobs\ImportArticlesJob;
use App\Models\Article;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Carbon;

class CategoryService {

    public static function getCategoryTree() {
        $parentCategories = Category::with(['childs'])->where([
            ['parent_id', '=', 0]
        ])->get()->makeHidden(['parent_id']);
        return $parentCategories;
    }
}
