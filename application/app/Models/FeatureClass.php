<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class FeatureClass extends Model
{
    public $timestamps = false;

    protected $table = 'feature_classes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key',
        'title',
        'slug',
    ];

    protected $visible = [
        'key',
        'title',
        'slug',
    ];

}
