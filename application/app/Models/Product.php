<?php

namespace App\Models;

use App\Jobs\ProcessScheduleJob;
use App\Services\PositionService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Product extends Model
{
    protected $table = 'products';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var string[]
     */
    protected $casts = [];

    /**
     * @var array
     */
    protected $appends = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'title',
        'description',
        'vendor',
        'slug',
    ];

    protected $visible = [
        'id',
        'title',
        'description',
        'vendor',
        'slug',
    ];

    protected $with = [

    ];

    /**
     *
     */
    protected static function boot()
    {
        parent::boot();

        static::created(function (Product $product) {
        });

        static::updated(function (Product $product) {

        });
    }

    /**
     * get the articles of
     * @return BelongsToMany Article
     */
    public function articles()
    {
        return $this->belongsToMany(Article::class, 'product_articles');
    }

}
