<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ArticleFeature extends Model
{
    public $timestamps = false;
    protected $table = 'article_features';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'article_id',
        'feature_id',
        'feature_value',
    ];

    /**
     * @var string[]
     */
    protected $visible = [
        'name',
        'value',
    ];

    protected $appends = [
        'name',
        'value',
        'featurekey',
        'featureSlug',
        'featureValueSlug',
    ];

    /**
     * @var array
     */
    protected $with = [
        'feature'
    ];

    public function getFeatureSlugAttribute() {
        return $this->feature->slug;
    }

    public function getFeatureValueSlugAttribute() {
        return $this->featureValue->slug;
    }

    /**
     * @return string
     */
    public function getFeatureKeyAttribute () {
        return $this->feature->key;
    }

    /**
     * @return string
     */
    public function getNameAttribute () {
        return $this->feature->title;
    }

    /**
     * @return string
     */
    public function getValueAttribute () {
        switch ($this->feature->type) {
            case 'bool':
                return (bool) $this->featureValue->title;
            case 'float':
                return (float) $this->featureValue->title;
            case 'integer':
                return (int) $this->featureValue->title;
            default:
                return $this->featureValue->title;
        }
    }

    /**
     * get the feature of
     * @return HasOne Feature
     */
    public function feature()
    {
        return $this->hasOne(Feature::class, 'id', 'feature_id');
    }

    /**
     * get the feature of
     * @return HasOne FeatureValue
     */
    public function featureValue()
    {
        return $this->hasOne(FeatureValue::class, 'id', 'feature_value');
    }

}
