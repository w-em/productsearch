<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ArticleGroup extends Model
{
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key',
        'title',
        'etim_name',
        'slug'
    ];

    protected $visible = [
        'key',
        'title',
        'etim_name',
        'slug'
    ];

    /**
     * get the articles of
     * @return HasMany ArticleFeatures
     */
    public function articles()
    {
        return $this->hasMany(Article::class, 'group_id', 'id');
    }
}
