<?php

namespace App\Models;

use App\Models\System\AbstractSystemItem;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Config;
use Laravel\Scout\Searchable;
use TeamTNT\TNTSearch\Indexer\TNTIndexer;

class Article extends Model
{
  use Searchable;

  const TRIGRAMS_FIELDS = ['short_description', 'article_number', 'ean'];

  public $timestamps = false;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'id',
    'article_number',
    'vendor',
    'title',
    'ean',
    'price',
    'keywords',
    'group_id',
    'short_description',
    'long_description',
    'slug',
    'transport',
    'online',
    'created_at',
    'updated_at',
    'n_grams',
  ];

  protected $visible = [
    'id',
    'vendor',
    'article_number',
    'title',
    'ean',
    'price',
    'short_description',
    // 'long_description',
    'medias',
    'slug',
    // 'categories',
    // 'properties',
    // 'features',
    // 'transport',
    'online',
  ];

  protected $appends = [
    'medias',
    'baseFeatures',
    'additionalFeatures',
    'electricFeatures',
    'ambientFeatures',
    'restFeatures',
    'properties'
  ];

  /**
   * @var string[]
   */
  protected $casts = [
    'transport' => 'array'
  ];

  /**
   * The attributes that should be mutated to dates.
   *
   * @var array
   */
  protected $dates = [
    'created_at',
    'updated_at'
  ];

  /**
   * @var array
   */
  protected $with = [

  ];

  /**
   * The "booting" method of the model.
   *
   * @return void
   */
  protected static function boot()
  {
    parent::boot();

    if (self::isLiveServer()) {
      static::addGlobalScope('online', function (Builder $builder) {
        $builder->where('online', '=', 1);
      });
    }
  }

  public static function isLiveServer () {
    if (!app()->runningInConsole()) {
      return parse_url(Config::get('app.url'))['host'] === request()->getHttpHost() && parse_url(Config::get('app.stage_url'))['host'] !== request()->getHttpHost();
    }
    return false;
  }

  /**
   * get the articles of
   * @return HasMany ArticleFeatures
   */
  public function features()
  {
    return $this->hasMany(ArticleFeature::class, 'article_id', 'id');
  }

  public function categories()
  {
    return $this->belongsToMany(Category::class, 'article_categories');
  }

  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
   */
  public function products()
  {
    return $this->belongsToMany(Product::class, 'product_articles');
  }

  public function getPropertiesAttribute()
  {
    $result = [];
    $result['main'] = [
      'values' => $this->baseFeatures
    ];
    $result['additional'] = [
      'values' => $this->additionalFeatures
    ];
    $result['electric'] = [
      'values' => $this->electricFeatures
    ];
    $result['ambient'] = [
      'values' => $this->ambientFeatures
    ];
    $result['rest'] = [
      'values' => $this->restFeatures
    ];
    return $result;
  }

  protected function getFeaturesByList($attrKeys)
  {
    $features = $this->features;
    $results = [];

    foreach ($attrKeys as $attrKey) {
      $results[$attrKey] = [
        'key' => $attrKey,
        'name' => trans('properties.' . $attrKey),
        'value' => '-'
      ];
    }

    foreach ($features->makeVisible(['featureSlug', 'featureValueSlug']) as $feature) {
      if (key_exists($feature->featureKey, $results)) {
        $results[$feature->featureKey]['key'] = $feature->featureSlug;
        $results[$feature->featureKey]['value'] = $feature->value;
      }
    }

    if (self::isLiveServer()) {
      foreach ($results as $key => $result) {
        if ($result['value'] === '-') {
          unset($results[$key]);
        }
      }
    }

    return array_values($results);
  }

  public function getRestFeaturesAttribute()
  {
    $arr = array_merge(
      Feature::FEATURES_MAIN,
      Feature::FEATURES_ADDITIONAL,
      Feature::FEATURES_ELECTRIC,
      Feature::FEATURES_AMBIENT,
      Feature::FEATURES_ENVIROMENT);

    $features = $this->features;
    $results = [];

    foreach ($features->makeVisible(['featureSlug', 'featureValueSlug']) as $feature) {
      if (!key_exists($feature->featureSlug, $arr)) {
        $results[] = [
          'key' => $feature->featureSlug,
          'name' => trans('properties.' . $feature->featureSlug),
          'value' => $feature->value
        ];
      }
    }
    return $results;
  }

  public function getBaseFeaturesAttribute()
  {
    return $this->getFeaturesByList(Feature::FEATURES_MAIN);
  }

  public function getAdditionalFeaturesAttribute()
  {
    return $this->getFeaturesByList(Feature::FEATURES_ADDITIONAL);
  }

  public function getElectricFeaturesAttribute()
  {
    return $this->getFeaturesByList(Feature::FEATURES_ELECTRIC);
  }

  public function getAmbientFeaturesAttribute()
  {
    return $this->getFeaturesByList(Feature::FEATURES_AMBIENT);
  }

  /**
   * @return array
   */
  public function getMediasAttribute()
  {
    $result = [];
    $references = MediaReference::with('media')->where([
      ['entity_id', '=', $this->id],
      ['entity_type', '=', self::class],
    ])
      // ->whereIn('field', ['bestellbild', 'fotorealistika', 'seitenansicht', 'skizze'])
      ->orderBy('sorting')
      ->get();

    foreach ($references as $reference) {
      $result[] = [
        'type' => $reference->field,
        'path' => $reference->media->path
      ];
    }

    return $result;
  }

  /**
   * Get the indexable data array for the model.
   *
   * @return array
   */
  public function toSearchableArray()
  {
    $categories = [];
    foreach ($this->categories as $category) {
      $categories[] = $category->title;
    }
    return [
      'id' => $this->id,
      'title' => $this->title,
      // 'titleNgrams' => utf8_encode((new TNTIndexer)->buildTrigrams($this->title)),
      'article_numberNgrams' => utf8_encode((new TNTIndexer)->buildTrigrams($this->article_number)),
      'short_description' => $this->short_description,
      //'long_description' => $this->long_description,
      'article_number' => $this->article_number,
      'category' => implode(' ', $categories),
      'ean' => $this->ean,
      'keywords' => $this->keywords,
    ];
  }
}
