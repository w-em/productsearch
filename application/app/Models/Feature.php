<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Feature extends Model
{
    public $timestamps = false;
    const FEATURES_MAIN = [
        'EF000008', // Breite [mm]
        'EF000040', // Höhe [mm]
        'EF000049', // Tiefe [mm]
        'WF000025', // Bauhöhe
        'WF000026', // Feldbreite
        'EF000266', // Anzahl Reihen
        'WF000002', // Anzahl Teilungseinheiten
        'WF000027', // Ausführung Messplatz
        'WF000028', // Ausführung Wandlerteil
        'WF000003', // Anzahl der N-Klemmen
        'WF000004', // Anzahl der N/N-Klemmen
        'WF000005', // Anzahl der PE-Klemmen
        'WF000006', // Anzahl der PE/N-Klemmen
        'WF000007', // Einsatzbereich
        'EF000007', // Farbe
        'EF000116', // RAL-Ton
    ];

    const FEATURES_ADDITIONAL = [
        'EF000003', // Schrankmontage
        'EF000537', //  Kabeleinführung
        'WF000008', // Anzahl Türen
        'WF000009', // Türmaterial
        'EF009212', // Türausführung
        'WF000011', // Türöffnungsseite
        'WF000012', // Türöffnungswinkel [°]
        'EF000123', // Anzahl Schlösser
        'WF000013', // Montagehalterung
        'EF001596', // Gehäusematerial
        'WF000014', // Normen
         // Langtext
    ];

    const FEATURES_ELECTRIC = [
        'EF007682', // Bemessungsspannung AC Un [V]
        'WF000029', // Bemessungsstoßspannungsfestigkeit Uimp [kV]
        'EF008873', // Nennstrom In [A]
        'WF000030', // Bemessungsbetriebstrom Inc [A]
        'WF000031', // Bemessungsstoßstromfestigkeit Icc [A]
        'WF000032', // Bemessungsstoßsstromfestigkeit Ipk [kA]
        'WF000033', // Bemessungskurzzeitstromfestigkeit Icw (1s) [kA]
        'WF000015', // Bemessungsfrequenz fn [Hz]
        'WF000034', // RDF bei Dauerbelastung
        'WF000035', // Verlustleistung [W]
        'EF005474', // IP-Schutzart
        'EF002569', // Überspannungskategorie
        'EF002570', // Verschmutzungsgrad nach IEC 61010-1
    ];

    const FEATURES_AMBIENT = [
        'WF000016', // Umgebungstempratur Mittelwert [°C]
        'WF000017', // Umgebungstemperatur bei Betrieb min. [°C]
        'WF000018', // Umgebungstemperatur bei Betrieb max. [°C]
        'WF000036', // Feuer Beständigkeit [°C]
        'WF000037', // Relative Luftfeuchtigkeit dauerhaft [%]
        'WF000038', // Relative Luftfeuchtigkeit kurzzeitig [%]
    ];

    const FEATURES_LOGISTIC = [
        'vpe', // Verpackungseinheit (VPE)
        'WF000019', // VPE auf Palette
        'WF000020', // Verpackungsabmessungen Breite [mm]
        'WF000021', // Verpackungsabmessungen Höhe [mm]
        'WF000022', // Verpackungsabmessungen Tiefe [mm]
        'WF000023', // Verpackungsgewicht [kg]
        'ean', // EAN Code
        'zolltarif_nr', // Zolltarif Nr.
    ];

    const FEATURES_ENVIROMENT = [
        'WF000024', // EU-RoHS-Richtlinie
    ];

    protected $table = 'features';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key',
        'title',
        'type',
        'slug',
        'parent',
        'category'
    ];

    protected $visible = [
        'title',
        'type',
        'slug',
        'options'
    ];

    protected $with = [
        'options'
    ];

    public function options()
    {
        return $this->hasMany(FeatureValue::class, 'parent', 'name');
    }
}
