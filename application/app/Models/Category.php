<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'title',
        'parent_id',
        'slug',
    ];

    protected $visible = [
        'id',
        'title',
        'childs',
        'slug'
    ];

    /**
     * @var string[]
     */
    protected $with = [
    ];

    /**
     * Get the childs for the model.
     *
     * @return string
     */
    public function childs() {
        return $this->hasMany(Category::class,'parent_id','id') ;
    }
}
