<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MediaReference extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'entity_id',
        'media_id',
        'entity_type',
        'field',
        'sorting',
    ];

    public function media () {
        return $this->hasOne(Media::class, 'id', 'media_id');
    }
}
