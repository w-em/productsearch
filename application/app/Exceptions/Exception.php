<?php

namespace App\Exceptions;


class Exception extends \Exception
{

    const STATUS_CODE = 404;

    protected $data = null;
    /**
     * Exception constructor.
     * @param string $message
     * @param int $code
     * @param Exception|null $previous
     */
    public function __construct($message = '', $code = 0, Exception $previous = null) {

        if(static::STATUS_CODE) {
            $code = static::STATUS_CODE;
        }

        // sicherstellen, dass alles korrekt zugewiesen wird
        parent::__construct($message, $code, $previous);
    }

    public function setData ($data) {
        $this->data = $data;
        return $this;
    }

    public function getData () {
        return $this->data;
    }

}
