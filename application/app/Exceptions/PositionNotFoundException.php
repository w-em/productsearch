<?php

namespace App\Exceptions;


class PositionNotFoundException extends Exception
{
    const STATUS_CODE = 404;

    public function __construct() {
        // sicherstellen, dass alles korrekt zugewiesen wird
        parent::__construct('Position not found', self::STATUS_CODE);
    }
}
