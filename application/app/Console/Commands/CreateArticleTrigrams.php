<?php

namespace App\Console\Commands;

use App\Models\Article;
use Illuminate\Console\Command;
use TeamTNT\TNTSearch\TNTSearch;
use TeamTNT\TNTSearch\Indexer\TNTIndexer;

class CreateArticleTrigrams extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'import:article:trigrams';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Creates an index of article trigrams';

  /**
   * @var TNTIndexer|null
   */
  protected $tnt = null;
  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->tnt = new TNTIndexer;
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    $this->createArticleTrigrams();

    $this->indexTriagrams();

  }

  protected function createArticleTrigrams () {
    $this->info("Creating and saving article trigrams");
    $articles = Article::all();
    foreach ($articles as $article) {
      $article->trigrams_article_number = $this->getNGrams($article->article_number);
      $article->trigrams_ean = $this->getNGrams($article->ean);
      $article->trigrams_keywords = $this->getNGrams($article->keywords);
      $article->trigrams_short_description = $this->getNGrams($article->short_description);
      $article->save();
    }

    $this->info("Creating and saving article trigrams: success");
  }

  protected function getNGrams ($word) {
    return utf8_encode($this->tnt->buildTrigrams(strip_tags($word)));
  }

  protected function indexTriagrams () {
    $this->info("Creating index of article trigrams");
    $tnt = new TNTSearch;
    $driver = config('database.default');
    $config = config('scout.tntsearch') + config("database.connections.$driver");
    $tnt->loadConfig($config);
    $tnt->setDatabaseHandle(app('db')->connection()->getPdo());

    foreach (Article::TRIGRAMS_FIELDS as $fieldname) {
      $indexer = $tnt->createIndex('article_ngrams_' . $fieldname . '.index');
      $indexer->query('SELECT id, trigrams_'. $fieldname .' FROM articles;');
      $indexer->setLanguage('no');
      $indexer->run();
    }


  }
}
