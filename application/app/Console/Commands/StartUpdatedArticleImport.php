<?php

namespace App\Console\Commands;

use App\Services\ImportService;
use Illuminate\Console\Command;

class StartUpdatedArticleImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:article:updated';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start the importer';

    /**
     * @var ImportService
     */
    protected $importService = null;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ImportService $importService)
    {

        $this->importService = $importService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->importService::startUpdatedArticleImport();
    }
}
