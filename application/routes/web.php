<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group([
    'middleware' => ['web']
], function () {
    Route::get('/', 'IndexController@indexAction');
    Route::get('/test', 'TestController@index');
    Route::get('/article', 'IndexController@search');
    Route::get('/category', 'IndexController@getCategories');
    Route::get('/basecategory', 'IndexController@getBaseCategories');
    Route::get('/filter', 'IndexController@getFilters');
    Route::get('/datasheet/multiple', 'DatasheetController@multiple');
    Route::get('/datasheet/{id?}', 'DatasheetController@getById')->where('id', '[\/\w\.-]*');
    Route::get('/article/suggest', 'IndexController@getSuggestion');
    Route::get('/article/{id?}', 'IndexController@getById')->where('id', '[\/\w\.-]*');
    Route::get('/img/{path}', 'ImageController@getByPath')->where('path', '.*');
    Route::get('/{pattern?}', 'IndexController@indexAction')->where('pattern', '[\/\w\.-]*');
});

Route::get('/home', 'HomeController@index')->name('home');
