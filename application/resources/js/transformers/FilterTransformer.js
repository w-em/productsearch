/* ============
 * Product Transformer
 * ============
 *
 * The transformer for the Product.
 */
import Transformer from '@/transformers/Transformer'
let getSlug = require('speakingurl')

export default class FilterTransformer extends Transformer {
  /**
   * Method used to transform a fetched Account.
   *
   * @param data The fetched Account.
   *
   * @returns {Object} The transformed Account.
   */
  static fetch (item) {
    return new Promise((resolve) => {
      this.fetchOptions(item.options).then((options) => {
        resolve({
          path: item.slug,
          label: item.label,
          selected: [],
          options: options
        })
      })
    })
  }

  static fetchOptions (values) {
    return new Promise((resolve) => {
      let options = []
      const length = values.length - 1
      values.forEach((type, index) => {
        options.push({
          value: type.slug,
          count: type.count,
          label: type.label
        })

        if (index === length) {
          resolve(options)
        }
      })
    })
  }
}
