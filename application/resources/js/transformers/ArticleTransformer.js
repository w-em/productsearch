/* ============
 * Article Transformer
 * ============
 *
 * The transformer for the Article.
 */
import Transformer from '@/transformers/Transformer'

export default class FilterTransformer extends Transformer {
  /**
   * Method used to transform a fetched Account.
   *
   * @param data The fetched Account.
   *
   * @returns {Object} The transformed Account.
   */
  static fetch (item) {
    const data = {
      id: item.id,
      ean: item.ean,
      articleNumber: item.article_number,
      longDescription: item.long_description,
      shortDescription: item.short_description,
      medias: [],
      categories: item.categories,
      transport: item.transport,
      price: parseFloat(item.price),
      properties: item.properties,
      features: item.features,
      slug: item.slug,
      title: item.title,
      vendor: item.vendor,
      firstImage: null,
      assemblyInstructions: item.medias.find(x => x.type === 'montageanleitung'),
      sketch: item.medias.find(x => x.type === 'skizze'),
      online: parseInt(item.online) === 1
    }

    const medias = []
    const mediaOrdering = ['bestellbild', 'fotorealistika', 'masszeichnung', 'skizze', 'seitenansicht']
    mediaOrdering.forEach((name) => {
      const media = item.medias.find(x => x.type === name)
      if (media) {
        medias.push(media)
      }
    })

    data.medias = medias

    if (medias.length > 0) {
      data.firstImage = medias[0]
    }

    return data
  }
}
