import template from './compare.html'
import './compare.scss'
import ArticleApiService from '@/api/article.api.service' // include the ajax service
import ArticleTransformer from '@/transformers/ArticleTransformer'
export default {
  template,
  mixins: [],
  metaInfo () {
    return {
      title: 'Artikelvergleich',
      meta: [
        {
          vmid: 'description',
          name: 'description',
          content: 'Artikelvergleich'
        }
      ]
    }
  },
  data () {
    return {
      items: [],
      loading: true
    }
  },
  computed: {
    empty () {
      return this.$store.getters.compare.length === 0
    },
    compare () {
      return this.$store.getters.compare
    },
    properties () {
      if (this.items.length > 0 && this.loading === false) {
        return this.items[0].properties
      }
      return []
    },
    /**
     * return
     * @returns []
     */
    differentKeys () {
      const results = []
      const keys = ['main', 'additional', 'electric', 'ambient']

      keys.forEach((key) => {
        this.properties[key].values.forEach((propVal, index) => {
          const value = propVal.value
          this.items.forEach((item) => {
            if (item.properties[key].values[index].value !== value) {
              results.push(propVal.key)
            }
          })
        })
      })

      return results
    }
  },
  methods: {
    loadArticle (slug) {
      return new Promise((resolve) => {
        ArticleApiService.getById(slug).then(response => {
          resolve(ArticleTransformer.fetch(response))
        })
      })
    },
    load () {
      const self = this
      const result = []
      const promises = []
      const len = this.compare.length - 1
      self.loading = true

      if (this.compare.length === 0) {
        self.items = []
        self.$nextTick(() => {
          self.loading = false
        })

        return
      }

      this.compare.forEach((slug, index) => {
        promises.push(self.loadArticle(slug))
        if (index === len) {
          Promise.all(promises).then(responses => {
            const rLen = responses.length - 1
            responses.forEach((response, rIndex) => {
              result[rIndex] = response

              if (rIndex === rLen) {
                self.items = result
                self.loading = false
              }
            })
          })
        }
      })
    },
    getImagePath (image) {
      return '/img/' + image + '?w=450&h=450&fit=fill&border=15,ffffff'
    },
    goBackToResults () {
      if (this.$route.query.backUrl) {
        this.$router.push(this.$route.query.backUrl)
        return false
      }
      this.$router.go(-1)
      return false
    }
  },
  mounted () {
    this.load()
  },
  watch: {
    compare: {
      handler () {
        this.load()
      },
      deep: true
    }
  }
}
