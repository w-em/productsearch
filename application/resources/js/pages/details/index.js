import template from './detail.html'
import './detail.scss'
import ArticleApiService from '@/api/article.api.service' // include the ajax service
import DatasheetApiService from '@/api/datasheet.api.service'
import ArticleTransformer from '@/transformers/ArticleTransformer'
// import { ImageMagnifier } from 'vue-image-magnifier'
import compareMixin from '@/mixins/compare.mixins'
import favoriteMixin from '@/mixins/favorite.mixins'
const previewImage = require('@/assets/no-preview.jpg')

export default {
  template,
  metaInfo () {
    return {
      title: this.loading ? 'wird geladen' : this.item ? this.item.shortDescription : 'Fehler',
      meta: [
        {
          vmid: 'description',
          name: 'description',
          content: this.loading ? 'wird geladen' : this.item ? this.item.shortDescription : 'Fehler'
        }
      ]
    }
  },
  props: {
    slug: { // id is given line 67 in path.js props.id = +props.id
      type: String,
      required: true
    }
  },
  mixins: [compareMixin, favoriteMixin],
  data () {
    return {
      selectedImage: null,
      loading: true,
      searchTerm: '',
      item: null,
      tab: 'data',
      currentDate: new Date().toLocaleString(),
      previewImage: previewImage,
      currentSlideIndex: 0,
      thumbSwiperOptions: {
        // direction: 'vertical',
        slidesPerView: 4,
        centeredSlides: false,
        spaceBetween: 5,
        pagination: {
          el: '.thump-swiper-pagination'
        }
        // Some Swiper option/callback...
      },
      galery: false,
      swiperOptions: {
        // direction: 'vertical',
        slidesPerView: 1,
        spaceBetween: 0,
        pagination: {
          el: '.swiper-pagination'
        }
        // Some Swiper option/callback...
      },
      zoomerOptions: {
        zoomFactor: 3, // scale for zoomer
        pane: 'container', // three type of pane ['pane', 'container-round', 'container']
        hoverDelay: 300, // how long after the zoomer take effect
        namespace: 'zoomer', // add a namespace for zoomer component, useful when on page have mutiple zoomer
        move_by_click: false, // move image by click thumb image or by mouseover
        scroll_items: 4, // thumbs for scroll
        choosed_thumb_border_color: '#bbdefb', // choosed thumb border color
        scroller_button_style: 'line',
        scroller_position: 'left',
        zoomer_pane_position: 'right'
      }
    }
  },
  computed: {
    isPreviewServer () {
      return this.$store.getters.isPreviewServer
    },
    images () {
      let results = []
      let images = this.item.medias.filter(x => x.type !== 'montageanleitung')
      if (images.length > 0) {
        results = images
      }
      return results
    },
    image () {
      if (this.item.medias.length > 0) {
        if (this.selectedImage) {
          return '/img/' + this.selectedImage + '?w=750&h=750&fit=fill&border=15,bbdefb,shrink'
        } else {
          const images = this.images
          if (images.length > 0) {
            return '/img/' + images[0].path + '?w=750&h=750&fit=fill&border=15,bbdefb,shrink'
          }
          return '/images/no-preview.jpg'
        }
      }
      return previewImage
    },
    firstImage () {
      const images = this.images
      if (images.length > 0) {
        return '/img/' + images[0].path + '?w=750&h=750&fit=fill&border=15,ffffff'
      }
      return '/images/no-preview.jpg'
    }
  },
  mounted () {
    this.load()
  },
  methods: {
    openGalery (index) {
      const self = this
      self.galery = true
    },
    onClickThumb (index) {
      this.currentSlideIndex = index
      this.$refs.slider.swiperInstance.slideTo(index)
    },
    getImagePath (path) {
      return '/img/' + path + '?w=450&h=450&fit=fill&border=10,ffffff,shrink'
    },
    getGaleryImagePath (path) {
      return '/img/' + path + '?w=950&h=950&fit=fill&border=10,ffffff,shrink'
    },
    getThumbImagePath (path) {
      return '/img/' + path + '?w=120&h=120&fit=fill&border=10,ffffff,shrink'
    },
    downloadDatasheet () {
      window.open(DatasheetApiService.getApiBasePath(this.slug), '_blank')
    },
    goBackToResults () {
      if (this.$route.query.backUrl) {
        this.$router.push(this.$route.query.backUrl)
        return false
      }
      this.$router.go(-1)
      return false
    },
    load () {
      const me = this
      me.loading = true

      ArticleApiService.getById(this.slug).then((result) => {
        me.item = ArticleTransformer.fetch(result)
      }).finally(() => {
        me.loading = false
      })
    }
  }
}
