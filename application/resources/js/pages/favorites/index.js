import template from './favorites.html'
import './favorites.scss'
import ArticleApiService from '@/api/article.api.service' // include the ajax service
import ArticleTransformer from '@/transformers/ArticleTransformer'
import DatasheetApiService from '@/api/datasheet.api.service'
export default {
  template,
  mixins: [],
  metaInfo () {
    return {
      title: 'Ihre Favoriten',
      meta: [
        {
          vmid: 'description',
          name: 'description',
          content: 'Ihre Favoriten'
        }
      ]
    }
  },
  data () {
    return {
      items: [],
      loading: true
    }
  },
  computed: {
    favorites () {
      return this.$store.getters.favorite
    }
  },
  methods: {
    downloadDatasheets () {
      const slugs = []
      this.favorites.forEach((favorite) => {
        slugs.push('id[]=' + favorite.slug)
      })

      if (slugs.length > 0) {
        window.open(DatasheetApiService.getApiBasePath('multiple') + '?' + slugs.join('&'), '_blank')
      }
    },
    removeAll () {
      this.$store.commit('clearFavorite')
    },
    loadArticle (slug) {
      return new Promise((resolve) => {
        ArticleApiService.getById(slug).then(response => {
          resolve(ArticleTransformer.fetch(response))
        })
      })
    },
    load () {
      const self = this
      const result = []
      const promises = []
      const len = this.favorites.length - 1
      self.loading = true

      if (this.favorites.length === 0) {
        self.items = []
        self.$nextTick(() => {
          self.loading = false
        })

        return
      }

      this.favorites.forEach((item, index) => {
        promises.push(self.loadArticle(item.slug))
        if (index === len) {
          Promise.all(promises).then(responses => {
            const rLen = responses.length - 1
            responses.forEach((response, rIndex) => {
              result[rIndex] = response

              if (rIndex === rLen) {
                self.items = result
                self.loading = false
              }
            })
          })
        }
      })
    },
    goBackToResults () {
      if (this.$route.query.backUrl) {
        this.$router.push(this.$route.query.backUrl)
        return false
      }
      this.$router.go(-1)
      return false
    }
  },
  mounted () {
    this.load()
  },
  watch: {
    favorites: {
      handler (newval) {
        this.load()
      },
      deep: true
    }
  }
}
