import template from './search.html'
import './search.scss'
import listingMixin from '@/mixins/listing.mixin'
import types from '@/utils/types.utils'
import ArticleApiService from '@/api/article.api.service'
import CategoryApiService from '@/api/category.api.service'
import FilterApiService from '@/api/filter.api.service'
import ArticleTransformer from '@/transformers/ArticleTransformer'
import _ from 'lodash'

export default {
  template,
  mixins: [listingMixin],
  metaInfo () {
    return {
      title: 'ABN Produktsuche',
      meta: [
        {
          vmid: 'description',
          name: 'description',
          content: 'ABN Produktsuche'
        }
      ]
    }
  },
  data () {
    return {
      error: '',
      loading: false,
      categories: [],
      selectedCategories: [],
      treeSelectedCategories: [],
      filters: [],
      searchTerm: '',
      items: [],
      selected: [],
      baseCategories: null,
      selectedBaseCategory: null,
      filterPanels: {},
      categoryPanels: {},
      expandedFilterPanels: []
    }
  },
  computed: {
    paginatedData () {
      const start = (this.page * this.limit) - (this.limit - 1)
      const end = Math.min(start + this.limit - 1, this.total)
      return {
        start: start,
        end: end
      }
    },
    drawer: {
      get () {
        return this.$store.getters.drawer
      },
      set (val) {
        this.$store.commit('setDrawer', val)
      }
    },
    selectedFilters () {
      const results = {}
      this.filters.forEach((filter) => {
        results[filter.slug] = {}
        filter.options.forEach((option) => {
          results[filter.slug][option.slug] = this.isFilterSelected(filter.slug, option.slug)
        })
      })
      return results
    },
    compSelectedCategories () {
      const results = {}
      const flattenChilds = _.flatMap(this.categories, ({ slug, childs }) =>
        _.map(childs, child => ({ slug, ...child }))
      )

      this.categories.forEach(category => {
        results[category.slug] = this.selectedCategories.indexOf(category.slug) > -1
      })
      flattenChilds.forEach(category => {
        results[category.slug] = this.selectedCategories.indexOf(category.slug) > -1
      })
      return results
    }
  },
  created () {
    const self = this
    const query = this.$route.query

    if (query.term) {
      self.searchTerm = query.term
    }
  },
  mounted () {
    const self = this
    self.$root.$on('category::change', (value) => {
      self.changeCategory(value)
    })

    self.$root.$on('selectedFilter::change', (filter, value) => {
      self.changeFilter(filter, value)
    })
  },
  methods: {
    addToExpandedFilter (slug) {
      if (this.expandedFilterPanels.indexOf(slug) === -1) {
        this.expandedFilterPanels.push(slug)
      }
    },
    removeFromExpandedFilter (slug) {
      const index = this.expandedFilterPanels.findIndex(x => x === slug)
      if (index > -1) {
        this.expandedFilterPanels.splice(index, 1)
      }
    },
    getPanelStyle (filter) {
      if (this.isPanelExpanded(filter.slug) || filter.options.length <= 5) {
        return {}
      } else {
        return {
          height: '150px'
        }
      }
    },
    isPanelExpanded (slug) {
      return this.expandedFilterPanels.indexOf(slug) > -1
    },
    check: function (e) {
      e.cancelBubble = true
    },
    changeFilter (filterSlug, value) {
      const self = this
      const selectedFilters = JSON.parse(JSON.stringify(self.selected))
      const filterIndex = selectedFilters.findIndex(x => x[0] === filterSlug)

      if (filterIndex === -1) { // slug not found
        selectedFilters.push([filterSlug, value])
      } else { // slug is found
        // check value exists
        if (selectedFilters[filterIndex].indexOf(value) === -1) {
          selectedFilters[filterIndex].push(value)
        } else {
          selectedFilters[filterIndex].splice(selectedFilters[filterIndex].indexOf(value), 1)
          if (selectedFilters[filterIndex].length === 1) {
            selectedFilters.splice(filterIndex, 1)
          }
        }
      }

      self.selected = selectedFilters

      self.updateRoute({
        page: 1
      }, self.getFilterParams())
    },
    updateData (customData) {
      const self = this
      self.page = parseInt(customData.page, 10) || self.page
      self.limit = parseInt(customData.limit, 10) || self.limit
      self.term = customData.term
      self.sortBy = customData.sortBy || self.sortBy
      self.sortDirection = customData.sortDirection || self.sortDirection
      self.selectedCategories = []
      self.selectedBaseCategory = null

      if (customData.base) {
        self.selectedBaseCategory = customData.base
      }

      if (customData.categories) {
        const categories = types.isArray(customData.categories) ? customData.categories : [customData.categories]
        categories.forEach((item) => {
          if (self.selectedCategories.indexOf(item) === -1) {
            self.selectedCategories.push(item)
          }
        })
      }

      const filters = []
      if (customData.filters) {
        if (types.isArray(customData.filters)) {
          customData.filters.forEach((filter) => {
            if (types.isArray(filter)) {
              filters.push(filter)
            } else {
              filters.push(filter.split(','))
            }
          })
        } else {
          filters.push(customData.filters.split(','))
        }
      }
      self.selected = filters
    },
    executeSearch () {
      if (this.searchTerm.length === 0) {
        this.searchTerm = undefined
      }
      this.term = this.searchTerm
      const query = {}

      if (this.selectedBaseCategory) {
        query.base = this.selectedBaseCategory
      }

      this.updateRoute({
        term: this.term,
        page: 1
      }, query)
    },
    getList () {
      const self = this
      const params = this.getListingParams()

      self.loading = true
      self.error = ''

      Promise.all([
        self.loadArticles(params),
        self.loadCategories(params),
        self.loadFilters(params)
      ]).then((results) => {

      }).finally(() => {
        self.loading = false
      })
    },
    loadCategories (params) {
      const self = this

      return new Promise((resolve, reject) => {
        CategoryApiService.getList(params).then((result) => {
          let categories = []
          // const oldCategoryPanels = self.categoryPanels
          if (result.categories && result.categories.length > 0) {
            categories = result.categories
          }

          let newCategoryPanels = []
          self.categories = categories
          self.categories.forEach((f) => {
            newCategoryPanels[f.slug] = self.categoryPanels[f.slug] === 0 ? 0 : null
          })
          self.categoryPanels = newCategoryPanels
          resolve()
        }).catch((err) => {
          reject(err)
        })
      })
    },
    isFilterSelected (filter, value) {
      return !!this.selected.find(x => x[0] === filter && x.indexOf(value) > 0)
    },
    onFilterChange (key, value) {
      this.$root.$emit('selectedFilter::change', key, value)
    },
    loadFilters (params) {
      const self = this
      return new Promise((resolve, reject) => {
        FilterApiService.getList(params).then((result) => {
          self.filters = result.filters
          const panels = {}
          self.filters.forEach((f) => {
            panels[f.slug] = self.filterPanels[f.slug] === 0 ? 0 : null
          })

          self.filterPanels = panels
          resolve()
        })
      })
    },
    loadArticles (params) {
      const self = this
      return new Promise((resolve, reject) => {
        ArticleApiService.getList(params).then((result) => {
          self.total = result.total
          self.items = ArticleTransformer.fetchCollection(result.items)
          resolve()
        }).catch((error) => {
          reject(error)
        })
      })
    },
    onPageChange (opts) {
      this.page = opts.page
      this.limit = opts.limit
      this.updateRoute({
        page: this.page
      }, this.getFilterParams())
    },
    onBaseCategoryChange (event) {
      this.$nextTick(function () {
        const self = this
        const params = {}
        if (self.selectedBaseCategory) {
          params.base = self.selectedBaseCategory
        }
        self.updateRoute({
          page: 1
        }, params)
      })
    },
    changeCategory (value) {
      const self = this
      const query = self.$route.query
      const index = self.selectedCategories.indexOf(value)
      this.page = 1
      if (index === -1) {
        self.selectedCategories.push(value)
      } else {
        self.selectedCategories.splice(index, 1)
      }

      self.$nextTick(function () {
        const params = self.getFilterParams()
        self.updateRoute({
          page: this.page,
          term: query.term || this.term
        }, params)
      })
    },
    getFilterParams (disableFilters = false) {
      const self = this
      const params = {}

      if (self.selectedBaseCategory) {
        params.base = self.selectedBaseCategory
      }

      if (self.selectedCategories.length > 0) {
        params.categories = self.selectedCategories
      } else {
        params.categories = undefined
      }
      if (self.selected.length > 0 && disableFilters === false) {
        params.filters = self.selected
      } else {
        params.filters = undefined
      }
      return params
    },
    onPaginationChange (val) {
      this.onPageChange({
        page: val,
        limit: this.limit
      })
    }
  }
}
