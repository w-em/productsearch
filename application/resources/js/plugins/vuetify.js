import Vue from 'vue'
import 'vuetify/dist/vuetify.min.css'
import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader

import Vuetify, {
  VCard,
  VApp,
  VAppBar,
  VBadge,
  VMain,
  VNavigationDrawer,
  VList,
  VListItem,
  VContainer,
  VRow,
  VCol,
  VIcon,
  VExpansionPanel,
  VExpansionPanels,
  VExpansionPanelHeader,
  VExpansionPanelContent,
  VCardActions,
  VCheckbox,
  VCardText,
  VCardTitle,
  VImg,
  VOverlay,
  VDialog,
  VSpacer,
  VToolbar,
  VBtn,
  VBtnToggle,
  VPagination,
  VSkeletonLoader,
  VItemGroup,
  VTextField,
  VTooltip,
  VCarousel,
  VCarouselItem,
  VSimpleTable
} from 'vuetify/lib'

const opts = {
  components: {
    VCard,
    VApp,
    VAppBar,
    VBadge,
    VMain,
    VNavigationDrawer,
    VList,
    VListItem,
    VContainer,
    VRow,
    VCol,
    VSimpleTable,
    VIcon,
    VExpansionPanel,
    VExpansionPanels,
    VExpansionPanelHeader,
    VExpansionPanelContent,
    VCardActions,
    VCheckbox,
    VCardText,
    VCardTitle,
    VImg,
    VOverlay,
    VDialog,
    VSpacer,
    VToolbar,
    VBtn,
    VBtnToggle,
    VPagination,
    VSkeletonLoader,
    VItemGroup,
    VCarousel,
    VCarouselItem,
    VTextField,
    VTooltip
  },
  iconfont: 'mdi',
  theme: {
    themes: {
      light: {
        primary: '#3dcd57'
      }
    }
  }
}

Vue.use(Vuetify, opts)

export default new Vuetify(opts)
