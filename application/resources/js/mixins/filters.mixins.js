import FilterTransformer from '../transformers/FilterTransformer'

export default {
  methods: {
    initFilters () {
      const self = this
      const datas = window.productsearch
      let promises = []

      if (datas) {
        if (datas.categories) {
          promises.push(self.getFilter('category', 'category', 'category', datas.categories, 0))
        }
        if (datas.maintenances) {
          promises.push(self.getFilter('maintenance', 'maintenance', 'maintenance', datas.maintenances, 0))
        }
        if (datas.thread_types) {
          promises.push(self.getFilter('thread_type', 'thread_type', 'thread_type', datas.thread_types, 1))
        }
        if (datas.series) {
          promises.push(self.getFilter('serie', 'serie', 'serie', datas.series, 0))
        }
        if (datas.dimensions) {
          promises.push(self.getFilter('dimension', 'dimension', 'dimension', datas.dimensions, 0))
        }
        if (datas.materials) {
          promises.push(self.getFilter('material', 'material', 'material', datas.materials, 0))
        }
        if (datas.drillings) {
          promises.push(self.getFilter('drilling', 'drillings', 'drilling', datas.drillings, 1))
        }
        if (datas.threads) {
          promises.push(self.getFilter('thread', 'thread', 'thread', datas.threads, 1))
        }
        if (datas.productLines) {
          promises.push(self.getFilter('line', 'line', 'line', datas.productLines, 0))
        }
      }

      Promise.all(promises).then((results) => {
        self.filters = results
      })
    },
    getFilter (field, label, path, values, article = false) {
      const self = this
      return new Promise((resolve) => {
        FilterTransformer.fetch(field, label, path, values, article).then((result) => {
          if (self.$route.query[result.path]) {
            result.selected = self.$route.query[result.path]
          }
          return resolve(result)
        })
      })
    }
  }
}
