export default {
  methods: {
    goToArticle (article) {
      this.$router.push({
        name: 'detail',
        params: {
          slug: article.slug
        },
        query: {
          backUrl: this.$route.fullPath
        }
      })
    }
  }
}
