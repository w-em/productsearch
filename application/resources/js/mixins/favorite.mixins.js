export default {
  methods: {
    onFavoriteChange (item) {
      if (this.favoriteSelected) {
        this.$store.commit('removeFromFavorite', this.item.slug)
      } else {
        this.$store.commit('addToFavorite', this.item.slug)
      }
    }
  },
  computed: {
    favoriteSelected: {
      get () {
        return this.$store.getters.favorite.find(x => x.slug === this.item.slug)
      },
      set (val) {

      }
    }
  }
}
