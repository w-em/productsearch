export default {
  methods: {
    onCompareChange (evt) {
      if (this.compareSelected) {
        this.$store.commit('removeFromCompare', this.item.slug)
      } else {
        if (this.$store.getters.compare.length < this.$store.getters.maxCompareItems) {
          this.$store.commit('addToCompare', this.item.slug)
        }
      }
    }
  },
  computed: {
    compareSelected: {
      get () {
        return this.$store.getters.compare.find(x => x === this.item.slug)
      },
      set (val) {
      }
    },
    compareDisabled () {
      return this.$store.getters.compare.length >= this.$store.getters.maxCompareItems && !!this.compareSelected === false
    }
  }
}
