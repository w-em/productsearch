/**
 * Define all of your application routes here
 * for more information on routes, see the
 * official documentation https://router.vuejs.org/en/
 */

export default [
  {
    path: '*',
    meta: {
      name: '',
      requiresAuth: false
    }
  },
  // This  allows you to have pages apart of the app but no rendered inside the dash
  {
    path: '/',
    name: 'index.index',
    component: () => import(/* webpackChunkName: "js/pages" */ '@/pages/list')
  },
  {
    path: '/detail/:slug',
    name: 'detail',
    props (route) {
      return { ...route.params }
    },
    component: () => import(/* webpackChunkName: "js/pages" */ '@/pages/details')
  },
  {
    path: '/favorites',
    name: 'favorites.index',
    component: () => import(/* webpackChunkName: "js/pages" */ '@/pages/favorites')
  },
  {
    path: '/compare',
    name: 'compare.index',
    component: () => import(/* webpackChunkName: "js/pages" */ '@/pages/compare')
  },
  {
    path: '/about',
    name: 'about',
    component: () => import(/* webpackChunkName: "js/pages" */ '@/pages/About.vue')
  }
]
