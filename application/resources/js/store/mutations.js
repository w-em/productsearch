// https://vuex.vuejs.org/en/mutations.html

export default {
  setDrawer (state, val) {
    state.drawer = val
  },
  setIsPreviewServer (state, val) {
    state.isPreviewServer = val
  },
  clearCompare (state) {
    state.compare = []
  },
  setSidebar (state, val) {
    state.sidebar = val
  },
  addToCompare (state, slug) {
    if (!state.compare.find(x => x === slug)) {
      state.compare.push(slug)
    }
  },
  removeFromCompare (state, slug) {
    const index = state.compare.findIndex(x => x === slug)
    if (index > -1) {
      state.compare.splice(index, 1)
    }
  },
  clearFavorite (state) {
    state.favorite = []
  },
  addToFavorite (state, slug) {
    if (!state.favorite.find(x => x.slug === slug)) {
      state.favorite.push({
        slug: slug,
        amount: 1
      })
    }
  },
  removeFromFavorite (state, slug) {
    const index = state.favorite.findIndex(x => x.slug === slug)
    if (index > -1) {
      state.favorite.splice(index, 1)
    }
  },
  changeFavoriteAmount (state, payload) {
    const index = state.favorite.findIndex(x => x.slug === payload.slug)
    if (index > -1) {
      state.favorite.splice(index, 1, payload)
    }
  }
}
