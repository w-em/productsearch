/**
 * Vuex
 *
 * @library
 *
 * https://vuex.vuejs.org/en/
 */

// Lib imports
import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'
// import localforage from 'localforage'

// Store functionality
import actions from './actions'
import getters from './getters'
import mutations from './mutations'
import state from './state'

const vuexLocalForage = new VuexPersistence({
  key: 'abn-storage',
  storage: window.sessionStorage,
  strictMode: false,
  asyncStorage: false,
  name: 'ABN-Storage'
})

Vue.use(Vuex)

// Create a new store
const store = new Vuex.Store({
  actions,
  getters,
  mutations,
  state,
  strict: true,

  plugins: [vuexLocalForage.plugin]
})

export default store
