import Vue from 'vue'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import vuetify from '@/plugins/vuetify' // path to vuetify export
import 'swiper/swiper-bundle.css'

import App from './App.vue'
// import './registerServiceWorker'
import './components'
import router from './router'
import store from './store'

Vue.use(VueAwesomeSwiper)
Vue.config.productionTip = true

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
