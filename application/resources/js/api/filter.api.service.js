import ApiService from './api.service'

/**
 * Gateway for the API end point "article"
 * @class
 * @extends ApiService
 */
class FilterApiService extends ApiService {
  constructor (apiEndpoint = 'filter') {
    super(apiEndpoint)
  }

  /**
   * Gets a list from the configured API end point using the page & limit.
   *
   * @param {String} term
   * @param {Array} queries
   * @returns {Promise<T>}
   */
  getList ({ term, queries }) {
    const requestHeaders = this.getBasicHeaders({})
    const params = {}
    if (term) {
      params.term = term
    }
    if (queries) {
      for (const key in queries) {
        params[key] = queries[key]
      }
    }

    return this.httpClient
      .get(this.getApiBasePath(), { params, headers: requestHeaders })
      .then((response) => {
        return ApiService.handleResponse(response)
      })
  }
}

export default new FilterApiService()
