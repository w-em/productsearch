import parseJsonApi from './jsonapi-parser.service'
import axios from 'axios'

/**
 * ApiService class which provides the common methods for our REST API
 * @class
 */
class ApiService {
  /**
   * @constructor
   * @param {AxiosInstance} httpClient
   * @param {String} apiEndpoint
   * @param {String} [contentType='application/vnd.api+json']
   */
  constructor (apiEndpoint, contentType = 'application/vnd.api+json') {
    this.httpClient = axios
    this.apiEndpoint = apiEndpoint
    this.contentType = contentType
  }

  /**
   * Getter & setter for the API end point
   * @type {String}
   */
  get apiEndpoint () {
    return this.endpoint
  }

  /**
   * @type {String}
   */
  set apiEndpoint (endpoint) {
    this.endpoint = endpoint
  }

  /**
   * Getter & setter for the http client
   *
   * @type {AxiosInstance}
   */
  get httpClient () {
    return this.client
  }

  /**
   * @type {AxiosInstance}
   */
  set httpClient (client) {
    this.client = client
  }

  /**
   * @type {String}
   */
  get contentType () {
    return this.type
  }

  /**
   * @type {String}
   */
  set contentType (contentType) {
    this.type = contentType
  }

  /**
   * Basic response handling.
   * Converts the JSON api data when the specific content type is set.
   *
   * @param response
   * @returns {*}
   */
  static handleResponse (response) {
    if (response.data === null || response.data === undefined) {
      return response
    }

    if (response.data.data === null || response.data.data === undefined) {
      return response.data
    }

    return response.data.data
  }

  /**
   * Parses a JSON api data structure to a simplified object.
   *
   * @param data
   * @returns {Object}
   */
  static parseJsonApiData (data) {
    return parseJsonApi(data)
  }

  /**
   * Gets a list from the configured API end point using the page & limit.
   *
   * @param {Number} page
   * @param {Number} limit
   * @param {String} sortBy
   * @param {String} sortDirection
   * @param {String} term
   * @param {Array} queries
   * @returns {Promise<T>}
   */
  getList ({
    page = 1,
    limit = 50,
    sortBy,
    sortDesc = false,
    term,
    queries
  }) {
    const requestHeaders = this.getBasicHeaders({})
    const params = { page, limit, sortBy, sortDesc }

    if (term) {
      params.term = term
    }

    if (queries) {
      for (const key in queries) {
        params[key] = queries[key]
      }
    }

    return this.httpClient
      .get(this.getApiBasePath(), { params, headers: requestHeaders })
      .then((response) => {
        return ApiService.handleResponse(response)
      })
  }

  /**
   * Get the detail entity from the API end point using the provided entity id.
   *
   * @param {Array} id
   * @param {Object} additionalParams
   * @param {Object} additionalHeaders
   * @returns {Promise<T>}
   */
  getListById (ids, additionalParams = {}, additionalHeaders = {}) {
    if (!ids.length === 0) {
      return Promise.reject(new Error('Missing required argument: ids'))
    }

    const params = additionalParams
    const headers = this.getBasicHeaders(additionalHeaders)
    params.ids = ids

    return this.httpClient
      .get(this.getApiBasePath('list'), {
        params,
        headers
      })
      .then((response) => {
        return ApiService.handleResponse(response)
      })
  }

  /**
   * Get the detail entity from the API end point using the provided entity id.
   *
   * @param {String|Number} id
   * @param {Object} additionalParams
   * @param {Object} additionalHeaders
   * @returns {Promise<T>}
   */
  getById (id, additionalParams = {}, additionalHeaders = {}) {
    if (!id) {
      return Promise.reject(new Error('Missing required argument: id'))
    }

    const params = additionalParams
    const headers = this.getBasicHeaders(additionalHeaders)

    return this.httpClient
      .get(this.getApiBasePath(id), {
        params,
        headers
      })
      .then((response) => {
        return ApiService.handleResponse(response)
      })
  }

  /**
   * Returns the URI to the API endpoint
   *
   * @param {String|Number} [id]
   * @param {String} [prefix='']
   * @returns {String}
   */
  getApiBasePath (id, prefix = '') {
    let url = ''

    if (prefix && prefix.length) {
      url += `${prefix}/`
    }

    if (id && (id.length > 0 || id > 0)) {
      return `${url}${this.apiEndpoint}/${id}`
    }

    return `${url}${this.apiEndpoint}`
  }

  /**
   * Get the basic headers for a request.
   *
   * @param additionalHeaders
   * @returns {Object}
   */
  getBasicHeaders (additionalHeaders = {}) {
    const basicHeaders = {
      Accept: this.contentType,
      'Content-Type': 'application/json'
    }

    return Object.assign({}, basicHeaders, additionalHeaders)
  }
}

export default ApiService
