<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_groups', function (Blueprint $table) {
            $table->id();
            $table->string('key')->index();
            $table->string('title')->index();
            $table->string('slug')->index();
        });

        Schema::create('features', function (Blueprint $table) {
            $table->id();
            $table->string('key')->index();
            $table->unsignedInteger('parent')->index();
            $table->string('title')->index();
            $table->string('type')->index();
            $table->string('slug')->index()->unique();
        });

        Schema::create('feature_classes', function (Blueprint $table) {
            $table->id();
            $table->string('key')->index();
            $table->string('title')->index();
            $table->string('slug')->index();
        });

        Schema::create('feature_values', function (Blueprint $table) {
            $table->id();
            $table->string('key')->index();
            $table->unsignedInteger('parent')->index();
            $table->string('title')->index();
            $table->string('slug')->index();
        });

        Schema::create('article_features', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('article_id')->index();
            $table->unsignedInteger('feature_id')->index();
            $table->unsignedInteger('feature_value')->index();
            $table->index(['article_id', 'feature_id', 'feature_value']);
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('features');
        Schema::dropIfExists('article_groups');
        Schema::dropIfExists('feature_classes');
        Schema::dropIfExists('feature_values');
        Schema::dropIfExists('article_features');
    }
}
