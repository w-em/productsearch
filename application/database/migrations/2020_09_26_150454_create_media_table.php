<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media', function (Blueprint $table) {
            $table->id();
            $table->string('path');
            $table->string('title');
            $table->string('file_type');
            $table->float('file_size', 15);
            $table->float('file_width');
            $table->float('file_height');
            $table->timestamps();
        });

        Schema::create('media_references', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('entity_id');
            $table->unsignedInteger('media_id');
            $table->string('entity_type'); // Model
            $table->string('field'); // field
            $table->unsignedInteger('sorting');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media');
        Schema::dropIfExists('media_references');
    }
}
