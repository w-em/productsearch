<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNgramArticle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('articles', function (Blueprint $table) {
          $table->text('trigrams_article_number')->after('transport');
          $table->text('trigrams_ean')->after('trigrams_article_number');
          $table->text('trigrams_keywords')->after('trigrams_ean');
          $table->text('trigrams_short_description')->after('trigrams_keywords');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('articles', function (Blueprint $table) {
        $table->dropColumn('trigrams_article_number');
        $table->dropColumn('trigrams_ean');
        $table->dropColumn('trigrams_keywords');
        $table->dropColumn('trigrams_title');
      });
    }
}
