<?php

$mw_apcExtensionLoaded = extension_loaded('apc');
$mw_apcuExtensionLoaded = extension_loaded('apcu');
$mw_apcAvailable = $mw_apcExtensionLoaded || $mw_apcuExtensionLoaded;
$mw_apcEnabled = ini_get('apc.enabled') == TRUE;

if (PHP_SAPI !== 'cli' && $mw_apcAvailable && $mw_apcEnabled) {
  $cacheType = 'array';
} else {
  $cacheType = 'array';
}

return [
    'cache-prefix' => '',

    'enabled' => env('MODEL_CACHE_ENABLED', true),

    'use-database-keying' => env('MODEL_CACHE_USE_DATABASE_KEYING', true),

    'store' => env('MODEL_CACHE_STORE', $cacheType),
];
