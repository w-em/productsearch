import ApiService from './api.service'

/**
 * Gateway for the API end point "article"
 * @class
 * @extends ApiService
 */
class ArticleApiService extends ApiService {
  constructor (apiEndpoint = '/article') {
    super(apiEndpoint)
  }

  /**
   * Gets a list from the configured API end point using the page & limit.
   *
   * @param {Number} page
   * @param {Number} limit
   * @param {String} sortBy
   * @param {String} sortDirection
   * @param {String} term
   * @param {Array} queries
   * @returns {Promise<T>}
   */
  getList ({
    page = 1,
    limit = 24,
    sortBy,
    sortDesc = false,
    term,
    queries
  }) {
    const requestHeaders = this.getBasicHeaders({})
    const params = { page, limit, sortBy, sortDesc }

    if (term) {
      params.term = term
    }

    if (queries) {
      for (const key in queries) {
        params[key] = queries[key]
      }
    }

    return this.httpClient
      .get(this.getApiBasePath(), { params, headers: requestHeaders })
      .then((response) => {
        return ApiService.handleResponse(response)
      })
  }
}

export default new ArticleApiService()
