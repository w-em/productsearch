import ApiService from './api.service'

/**
 * Gateway for the API end point "category"
 * @class
 * @extends ApiService
 */
class CategoryApiService extends ApiService {
  constructor (apiEndpoint = '/category') {
    super(apiEndpoint)
  }

  /**
   * Gets a list from the configured API end point using the page & limit.
   *
   * @param {Array} queries
   * @returns {Promise<T>}
   */
  getList ({ queries }) {
    const requestHeaders = this.getBasicHeaders({})
    const params = {}
    if (queries) {
      for (const key in queries) {
        params[key] = queries[key]
      }
    }

    return this.httpClient
      .get(this.getApiBasePath(), { params, headers: requestHeaders })
      .then((response) => {
        return ApiService.handleResponse(response)
      })
  }
}

export default new CategoryApiService()
