import ApiService from './api.service'

/**
 * Gateway for the API end point "datasheet"
 * @class
 * @extends ApiService
 */
class DatasheetApiService extends ApiService {
  constructor (apiEndpoint = '/datasheet') {
    super(apiEndpoint)
  }
}

export default new DatasheetApiService()
