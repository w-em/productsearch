import template from './detail.html'
import './detail.scss'
import ArticleApiService from '@/api/article.api.service' // include the ajax service
import DatasheetApiService from '@/api/datasheet.api.service' // include the ajax service
import ArticleTransformer from '@/transformers/ArticleTransformer'
import { ImageMagnifier } from 'vue-image-magnifier'

export default {
  template,
  props: {
    slug: { // id is given line 67 in path.js props.id = +props.id
      type: String,
      required: true
    }
  },
  components: {
    ImageMagnifier
  },
  data () {
    return {
      selectedImage: null,
      loading: true,
      searchTerm: '',
      item: null,
      swiperOptions: {
        // direction: 'vertical',
        slidesPerView: 4,
        spaceBetween: 15,
        pagination: {
          el: '.swiper-pagination'
        }
        // Some Swiper option/callback...
      },
      zoomerOptions: {
        zoomFactor: 3, // scale for zoomer
        pane: 'container', // three type of pane ['pane', 'container-round', 'container']
        hoverDelay: 300, // how long after the zoomer take effect
        namespace: 'zoomer', // add a namespace for zoomer component, useful when on page have mutiple zoomer
        move_by_click: false, // move image by click thumb image or by mouseover
        scroll_items: 4, // thumbs for scroll
        choosed_thumb_border_color: '#bbdefb', // choosed thumb border color
        scroller_button_style: 'line',
        scroller_position: 'left',
        zoomer_pane_position: 'right'
      }
    }
  },
  computed: {
    images () {
      return this.item.medias.filter(x => x.type !== 'montageanleitung')
    },
    image () {
      if (this.item.medias.length > 0) {
        if (this.selectedImage) {
          return '/img/' + this.selectedImage + '?w=750&h=750&fit=fill&border=15,ffffff'
        } else {
          const images = this.images
          if (images.length > 0) {
            return '/img/' + images[0].path + '?w=750&h=750&fit=fill&border=15,ffffff'
          }
          return '/images/no-preview.jpg'
        }
      }
      return '/images/no-preview.jpg'
    },
    sketch () {
      return this.item.medias.find(x => x.type === 'sketch')
    },
    assemblyInstructions () {
      return this.item.medias.find(x => x.type === 'montageanleitung')
    },
    firstImage () {
      const images = this.images
      if (images.length > 0) {
        return '/img/' + images[0].path + '?w=750&h=750&fit=fill&border=15,ffffff'
      }
      return '/images/no-preview.jpg'
    }
  },
  mounted () {
    this.load()
  },
  methods: {
    onClickThumb (path) {
      this.selectedImage = path
    },
    getThumbImagePath (path) {
      return '/img/' + path + '?w=120&h=120&fit=fill&border=10,ffffff'
    },
    downloadDatasheet () {
      console.log(DatasheetApiService.getApiBasePath(this.slug))
      window.open(DatasheetApiService.getApiBasePath(this.slug), '_blank')
    },
    goBackToResults () {
      if (this.$route.query.backUrl) {
        this.$router.push(this.$route.query.backUrl)
        return false
      }
      this.$router.go(-1)
      return false
    },
    load () {
      const me = this
      me.loading = true
      ArticleApiService.getById(this.slug).then((result) => {
        me.item = ArticleTransformer.fetch(result)
      }).finally(() => {
        me.loading = false
      })
    }
  }
}
