import template from './compare.html'
import './compare.scss'

export default {
  template,
  mixins: [],
  data () {
    return {
    }
  },
  computed: {
    items () {
      return this.$store.getters.compare
    },
    properties () {
      if (this.items.length > 0) {
        return this.items[0].properties
      }
      return []
    },
    /**
     * return
     * @returns []
     */
    differentKeys () {
      const results = []
      const keys = ['main', 'additional', 'electric', 'ambient']

      keys.forEach((key) => {
        this.properties[key].values.forEach((propVal, index) => {
          const value = propVal.value
          this.items.forEach((item) => {
            if (item.properties[key].values[index].value !== value) {
              results.push(propVal.key)
            }
          })
        })
      })

      return results
    }
  },
  methods: {
    getImagePath (image) {
      return '/img/' + image + '?w=450&h=450&fit=fill&border=15,ffffff'
    },
    goBackToResults () {
      if (this.$route.query.backUrl) {
        this.$router.push(this.$route.query.backUrl)
        return false
      }
      this.$router.go(-1)
      return false
    }
  }
}
