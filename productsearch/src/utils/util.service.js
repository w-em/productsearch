/**
 * @module core/service/utils
 */
import throttle from 'lodash/throttle'
import debounce from 'lodash/debounce'
import flattenDeep from 'lodash/flattenDeep'
import get from 'lodash/get'
import uuidV4 from 'uuid/v4'

import { deepCopyObject, getArrayChanges, getObjectDiff, hasOwnProperty, merge } from './object.utils'
import { warn } from './debug.utils'
import domUtils from './dom.utils'
import stringUtils from './string.utils'
import typesUtils, { isUndefined } from './types.utils'
import fileReaderUtils from './file-reader.utils'
import sortUtils from './sort.utils'

export const object = {
  deepCopyObject: deepCopyObject,
  hasOwnProperty: hasOwnProperty,
  getObjectDiff: getObjectDiff,
  getArrayChanges: getArrayChanges,
  merge: merge
}

export const debug = {
  warn: warn
}

export const dom = {
  getScrollbarHeight: domUtils.getScrollbarHeight,
  getScrollbarWidth: domUtils.getScrollbarWidth
}

export const string = {
  capitalizeString: stringUtils.capitalizeString,
  camelCase: stringUtils.camelCase,
  md5: stringUtils.md5
}

export const types = {
  isObject: typesUtils.isObject,
  isPlainObject: typesUtils.isPlainObject,
  isEmpty: typesUtils.isEmpty,
  isRegExp: typesUtils.isRegExp,
  isArray: typesUtils.isArray,
  isFunction: typesUtils.isFunction,
  isDate: typesUtils.isDate,
  isString: typesUtils.isString,
  isBoolean: typesUtils.isBoolean,
  isNumber: typesUtils.isNumber,
  isUndefined: isUndefined
}

export const fileReader = {
  readAsArrayBuffer: fileReaderUtils.readFileAsArrayBuffer,
  readAsDataURL: fileReaderUtils.readFileAsDataURL,
  readAsText: fileReaderUtils.readFileAsText
}

export const sort = {
  afterSort: sortUtils.afterSort
}

export const array = {
  flattenDeep: flattenDeep
}

export default {
  createId,
  throttle,
  debounce,
  get,
  object,
  debug,
  dom,
  string,
  types,
  fileReader,
  sort,
  array
}

/**
 * Returns a uuid string in hex format.
 *
 * @returns {String}
 */
function createId () {
  return uuidV4().replace(/-/g, '')
}
