/* ============
 * Article Transformer
 * ============
 *
 * The transformer for the Article.
 */
import Transformer from '@/transformers/Transformer'

export default class FilterTransformer extends Transformer {
  /**
   * Method used to transform a fetched Account.
   *
   * @param data The fetched Account.
   *
   * @returns {Object} The transformed Account.
   */
  static fetch (item) {
    const data = {
      id: item.id,
      ean: item.ean,
      articleNumber: item.article_number,
      longDescription: item.long_description,
      shortDescription: item.short_description,
      medias: item.medias,
      categories: item.categories,
      transport: item.transport,
      price: parseFloat(item.price),
      properties: item.properties,
      features: item.features,
      slug: item.slug,
      title: item.title,
      vendor: item.vendor,
      firstImage: null
    }

    const images = item.medias.filter(x => ['bestellbild', 'fotorealistika', 'seitenansicht'].indexOf(x.type) > -1)
    if (images.length > 0) {
      data.firstImage = images[0]
    }

    return data
  }
}
