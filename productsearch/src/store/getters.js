// https://vuex.vuejs.org/en/getters.html

// authorized lets you know if the token is true or not
export default {
  drawer: state => state.drawer,
  sidebar: state => state.sidebar,
  compare: state => state.compare,
  maxCompareItems: state => state.maxCompareItems
}
