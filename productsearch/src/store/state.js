// https://vuex.vuejs.org/en/state.html

export default {
  sidebar: true,
  drawer: true,
  maxCompareItems: 4,
  compare: []
}
