// https://vuex.vuejs.org/en/mutations.html

export default {
  setDrawer (state, val) {
    state.drawer = val
  },
  clearCompare (state) {
    state.compare = []
  },
  setSidebar (state, val) {
    state.sidebar = val
  },
  addToCompare (state, item) {
    if (!state.compare.find(x => x.id === item.id)) {
      state.compare.push(item)
    }
  },
  removeFromCompare (state, item) {
    const index = state.compare.findIndex(x => x.id === item.id)
    if (index > -1) {
      state.compare.splice(index, 1)
    }
  }
}
