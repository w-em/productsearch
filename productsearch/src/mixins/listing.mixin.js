import types from '@/utils/types.utils'
import debug from '@/utils/debug.utils'

export default {
  data () {
    return {
      page: 1,
      limit: 24,
      total: 0,
      sortBy: null,
      sortDirection: 'ASC',
      selection: [],
      term: undefined,
      disableRouteParams: false
    }
  },

  computed: {
    maxPage () {
      return Math.ceil(this.total / this.limit)
    },

    routeName () {
      return this.$route.name
    },

    selectionArray () {
      return Object.values(this.selection)
    },

    selectionCount () {
      return this.selectionArray.length
    }
  },

  created () {
    /*
    const query = this.$route.query
    for (let p in query) {
      if (['page', 'limit', 'sortBy', 'sortDirection', 'term'].indexOf(p) > -1) continue;

      let values = query[p]
      let selected = {
        key: p
      }

      if (types.isArray(values)) {
        selected.values = values
      }
      else if (types.isString(values)) {
        selected.values = [values]
      }

      this.selectedFilters.push(selected)
    } */
  },

  mounted () {
    const self = this
    if (self.disableRouteParams) {
      self.getList()
      return
    }
    const actualQueryParameters = self.$route.query

    // When no route information are provided
    if (types.isEmpty(actualQueryParameters)) {
      self.resetListing()
    } else {
      // otherwise update local data and fetch from server
      self.updateData(actualQueryParameters)
      self.getList()
    }
  },

  watch: {
    // Watch for changes in query parameters and update listing
    '$route' () {
      if (this.disableRouteParams) {
        return
      }

      const query = this.$route.query

      if (types.isEmpty(query)) {
        this.resetListing()
      }

      // Update data information from the url
      this.updateData(query)

      // Fetch new list
      this.getList()
    }
  },

  methods: {
    updateData (customData) {
      this.page = parseInt(customData.page, 10) || this.page
      this.limit = parseInt(customData.limit, 10) || this.limit
      this.term = customData.term || this.term
      this.sortBy = customData.sortBy || this.sortBy
      this.sortDirection = customData.sortDirection || this.sortDirection
    },
    updateRoute (customQuery, queryExtension = {}) {
      const self = this
      // Get actual query parameter
      const query = customQuery || this.$route.query
      const routeQuery = this.$route.query
      const newQuery = {}
      const limit = query.limit || this.limit
      if (limit !== 24) {
        newQuery.limit = limit
      }
      const page = query.page || this.page

      if (page !== 1) {
        newQuery.page = page
      }
      const sortBy = query.sortBy || this.sortBy
      if (sortBy) {
        newQuery.sortBy = sortBy
      }
      const sortDirection = query.sortDirection || this.sortDirection
      if (sortDirection !== 'ASC') {
        newQuery.sortDirection = sortDirection
      }
      const route = {
        name: this.$route.name,
        params: this.$route.params,
        query: {
          term: query.term || this.term,
          ...newQuery,
          ...queryExtension
        }
      }

      // If query is empty then replace route, otherwise push
      if (types.isEmpty(routeQuery)) {
        self.$router.replace(route)
      } else {
        self.$router.push(route)
      }
    },

    resetListing () {
      this.updateRoute({
        name: this.$route.name,
        query: {
          limit: this.limit,
          page: this.page,
          term: this.term,
          sortBy: this.sortBy,
          sortDirection: this.sortDirection
        }
      })
    },

    getListingParams () {
      if (this.disableRouteParams) {
        return {
          limit: this.limit,
          page: this.page,
          term: this.term,
          sortBy: this.sortBy,
          sortDirection: this.sortDirection
        }
      }
      // Get actual query parameter
      const query = this.$route.query

      const params = {
        limit: query.limit || 24,
        page: query.page || 1,
        term: query.term || '',
        sortBy: query.sortBy || this.sortBy,
        sortDirection: query.sortDirection || this.sortDirection
      }
      const customQuery = {}
      if (this.selectedCategories.length > 0) {
        customQuery.categories = this.selectedCategories
      }

      if (this.selected.length > 0) {
        customQuery.filters = JSON.stringify(this.selected)
      }

      if (this.selectedBaseCategory) {
        customQuery.base = this.selectedBaseCategory
      }

      params.queries = customQuery

      return params
    },

    updateSelection (selection) {
      this.selection = selection
    },

    onPageChange (opts) {
      this.page = opts.page
      this.limit = opts.limit
      if (this.disableRouteParams) {
        this.getList()
        return
      }
      this.updateRoute({
        page: this.page
      })
    },

    onSearch (value) {
      if (value.length === 0) {
        value = undefined
      }
      this.term = value

      if (this.disableRouteParams) {
        this.page = 1
        this.getList()
        return
      }

      this.updateRoute({
        term: this.term,
        page: 1
      })
    },

    onSortColumn (column) {
      if (this.disableRouteParams) {
        if (this.sortBy === column.dataIndex) {
          this.sortDirection = (this.sortDirection === 'ASC' ? 'DESC' : 'ASC')
        } else {
          this.sortDirection = 'ASC'
          this.sortBy = column.dataIndex
        }
        this.getList()
        return
      }

      if (this.sortBy === column.dataIndex) {
        this.updateRoute({
          sortDirection: (this.sortDirection === 'ASC' ? 'DESC' : 'ASC')
        })
      } else {
        this.updateRoute({
          sortBy: column.dataIndex,
          sortDirection: 'ASC'
        })
      }
      this.updateRoute()
    },

    onRefresh () {
      this.getList()
    },

    getList () {
      debug.warn(
        'Listing Mixin',
        'When using the listing mixin you have to implement your custom "getList()" method.'
      )
    }
  }
}
