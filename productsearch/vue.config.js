const path = require('path')

module.exports = {
  parallel: false,
  lintOnSave: true,
  productionSourceMap: process.env.NODE_ENV === 'production',
  integrity: process.env.NODE_ENV === 'production',
  configureWebpack: {
    resolve: {
      alias: {
        vue$: process.env.NODE_ENV === 'production' ? 'vue/dist/vue.min.js' : 'vue/dist/vue.js',
        vue: process.env.NODE_ENV === 'production' ? 'vue/dist/vue.min.js' : 'vue/dist/vue.js',
        src: path.resolve(__dirname, './src'),
        public: path.resolve(__dirname, '../public/'),
        assets: path.resolve(__dirname, './src/assets'),
        scss: path.resolve(__dirname, './src/scss'),
        '@': path.resolve(__dirname, './src'),
        '@api': path.resolve(__dirname, './src/api'),
        '@pages': path.resolve(__dirname, './src/pages'),
        '@mixins': path.resolve(__dirname, './src/mixins'),
        '@components': path.resolve(__dirname, './src/components'),
        '@transformers': path.resolve(__dirname, './src/transformers')
      }
    },
    plugins: [
    ]
  },
  chainWebpack: config => {
    config.module
      .rule('html')
      .test(/\.html|.twig$/)
      .use('html')
      .loader('html-loader')
      .end()
  },
  pluginOptions: {
  },
  transpileDependencies: [
    'vuetify'
  ],
  pages: {
    game: {
      entry: ['src/main.js'],
      template: process.env.NODE_ENV === 'production' ? 'src/indexLive.html' : 'src/index.html',
      filename: process.env.NODE_ENV === 'production' ? 'app.html' : 'index.html'
    }
  },
  css: {
    extract: true,
    sourceMap: false,
    loaderOptions: {
      sass: {
      }
    }
  },
  devServer: {
    open: true,
    overlay: {
      warnings: true,
      errors: true
    },
    proxy: {
      '/article': {
        target: 'http://localhost/article',
        changeOrigin: true,
        pathRewrite: {
          '^/article': ''
        }
      },
      '/filter': {
        target: 'http://localhost/filter',
        changeOrigin: true,
        pathRewrite: {
          '^/filter': ''
        }
      },
      '/category': {
        target: 'http://localhost/category',
        changeOrigin: true,
        pathRewrite: {
          '^/category': ''
        }
      },
      '/img': {
        target: 'http://localhost/img',
        changeOrigin: true,
        pathRewrite: {
          '^/img': ''
        }
      },
      '/basecategory': {
        target: 'http://localhost/basecategory',
        changeOrigin: true,
        pathRewrite: {
          '^/basecategory': ''
        }
      },
      '/storage': {
        target: 'http://localhost/storage',
        changeOrigin: true,
        pathRewrite: {
          '^/storage': ''
        }
      },
      '/datasheet': {
        target: 'http://localhost/datasheet',
        changeOrigin: true,
        pathRewrite: {
          '^/datasheet': ''
        }
      }
    }
  }
}
